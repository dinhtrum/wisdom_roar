import BaseModelRequest from '../libs/BaseModelRequest';
import AppConfig from '../utils/AppConfig';

export default class ShopRequest extends BaseModelRequest {
    getModelName() {
        return 'shop';
    }

    getShopDetail(id,params) {
        const url = '/public/shop/'+id;
        return this.get(url, params, true);
    }

}
