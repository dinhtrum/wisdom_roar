import BaseModelRequest from '../libs/BaseModelRequest';
import AppConfig from '../utils/AppConfig';

export default class UserRequest extends BaseModelRequest {

    getModelName() {
        return 'users';
    }

    login(username, password) {
        const params = {
            username,
            password,
        };
        return this.post('/login', params);
    }

    createUser(username, phone_number, password) {
        const params = {
            display_name: username,
            phone: phone_number,
            password,
        };
        return this.registerPost('/public/user/create', params);
    }

    getUser(id, params) {
        const url = '/public/user/' + id;
        return this.get(url, params, true);
    }

    getCurrentUser() {
        const url = '/security/user/profile';
        return this.get(url);
    }

    verifyRegister(phone, user_token) {
        const url = '/public/user/activate';
        const params = {
            phone, user_token,
        };
        return this.put(url, params);
    }

    getOTP(phone) {
        const url = '/public/user/forgot-password';
        return this.registerPost(url, {phone});
    }

    resetPassword(new_password, phone, verification_code) {
        const url = '/public/user/forgot-password';
        return this.put(url, {new_password, phone, verification_code});
    }

    changePassword(old_password, password, confirm_password, username) {
        const url = '/security/user/password/change';
        return this.security_put(url, {old_password, password, confirm_password, username});
    }

    uploadFile(body) {
        const url = '/file/image';
        return this.upload_file(url, body);
    }

    updateProfile(params) {
        const url = '/security/user';
        return this.security_put(url, params);
    }

}
