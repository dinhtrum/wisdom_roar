import BaseModelRequest from '../libs/BaseModelRequest';
import AppConfig from '../utils/AppConfig';

export default class CarRequest extends BaseModelRequest {
    getModelName() {
        return 'car';
    }

    getCarListConditions(params) {
        const url = '/public/post/search';
        return this.get(url, params, true);
    }

    getCarDetail(id, params) {
        const url = '/public/post/' + id;
        return this.get(url, params, true);
    }

    getReviewForModel(params) {
        const url = '/public/model-review/search';
        return this.get(url, params, true);
    }

}
