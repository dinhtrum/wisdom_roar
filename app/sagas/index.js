import {all} from 'redux-saga/effects';
import {watchFetchColors} from './colorsSaga';
import {watchFetchMasterData} from './masterdataSaga';
import {watchFetchBodyTypes} from './bodyTypesSaga';
import {watchChangeLanguage, watchFetchUserInformation, watchUpdateUserInformation} from './userInformationSaga';
import {watchFetchRegions} from './regionSaga';

export default function* rootSaga() {
    yield all([
        watchFetchUserInformation()
    ]);
}
