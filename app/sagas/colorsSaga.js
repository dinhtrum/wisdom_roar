import {
  call,
  fork,
  put,
  take,
} from 'redux-saga/effects';
import * as actionTypes from '../actions/types';
import HomeRequest from '../requests/HomeRequest';

async function loadPrices() {
  const request = new HomeRequest();
  return request.getColors();
}

function* fetchColors() {
  try {
    const response = yield call(loadPrices);
    yield put({ type: actionTypes.FETCH_COLORS_SUCCESS, payload: response.results });
  } catch (err) {
    console.log('colorSaga: Fetch color error', err);
    yield put({ type: actionTypes.CANCEL_FETCH_COLORS });
  }
}

export function* watchFetchColors() {
  while (yield take(actionTypes.FETCH_COLORS)) {
    yield fork(fetchColors);
  }
}
