import {
  call,
  fork,
  put,
  take,
} from 'redux-saga/effects';
import * as actionTypes from '../actions/types';
import HomeRequest from '../requests/HomeRequest';

async function loadRegion() {
  const request = new HomeRequest();
  return request.getRegions({country_id : 1});
}

async function loadProvinces(params) {
  let listProvinces = [];
  params.forEach(region =>{
    region.provinces.forEach(province =>{
      const {province_id} = province;
      listProvinces.push(province_id)
    })
  });
  const request = new HomeRequest();
  return request.getProvinces({province_id: listProvinces.join()});
}

function* fetchRegions() {
  try {
    const response = yield call(loadRegion);
    const provinces = yield call(loadProvinces,response.results);
    let listCities = [];
    response.results.forEach(item=>{
      listCities = [...listCities, ...item.provinces]
    });
    yield put({ type: actionTypes.FETCH_REGIONS_SUCCESS, payload: {listCities, provinces: provinces.results}});
  } catch (err) {
    console.log('regionSaga: Fetch region error', err);
    yield put({ type: actionTypes.CANCEL_FETCH_REGIONS });
  }
}

export function* watchFetchRegions() {
  while (yield take(actionTypes.FETCH_REGIONS)) {
    yield fork(fetchRegions);
  }
}
