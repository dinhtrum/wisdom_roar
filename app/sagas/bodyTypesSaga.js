import {
  call,
  fork,
  put,
  take,
} from 'redux-saga/effects';
import * as actionTypes from '../actions/types';
import HomeRequest from '../requests/HomeRequest';

async function loadBodyTypes() {
  const request = new HomeRequest();
  return request.getBodyTypes();
}

function* fetchBodyTypes() {
  try {
    const response = yield call(loadBodyTypes);
    yield put({ type: actionTypes.FETCH_BODY_TYPES_SUCCESS, payload: response.results });
  } catch (err) {
    console.log('colorSaga: Fetch color error', err);
    yield put({ type: actionTypes.CANCEL_FETCH_BODY_TYPES });
  }
}

export function* watchFetchBodyTypes() {
  while (yield take(actionTypes.FETCH_BODY_TYPES)) {
    yield fork(fetchBodyTypes);
  }
}
