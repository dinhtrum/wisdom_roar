import {
    call,
    fork,
    put,
    take,
} from 'redux-saga/effects';
import * as actionTypes from '../actions/types';
import HomeRequest from '../requests/HomeRequest';
import _ from 'lodash';
import React from 'react';

const COUNTRY = {

};

function getFlag(key) {
    return COUNTRY[key !== 'null' ? key.toLowerCase() : 'other'];
}

async function loadMasterData() {
    const request = new HomeRequest();
    return request.getManufacturers();
}

function groupByCountry(data) {
    return _.chain(data)
    // Group the elements of Array based on `color` property
        .groupBy('manufacturer_country_id')
        // `key` is group's name (color), `value` is the array of objects
        .map((value, key) => ({country_code: key, flag: getFlag(key), manufacturerList: value}))
        .value();
}

function* fetchMasterData() {
    try {
        const response = yield call(loadMasterData);
        const countries = groupByCountry(response.results);
        yield put({type: actionTypes.FETCH_MASTER_DATA_SUCCESS, payload: countries});
    } catch (err) {
        console.log('master dataSaga: Fetch master data error', err);
        yield put({type: actionTypes.CANCEL_FETCH_MASTER_DATA});
    }
}

export function* watchFetchMasterData() {
    while (yield take(actionTypes.FETCH_MASTER_DATA)) {
        yield fork(fetchMasterData);
    }
}

