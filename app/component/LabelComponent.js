import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {connect} from 'react-redux';
import ScaledSheet from '../libs/reactSizeMatter/ScaledSheet';
import I18n from '../i18n/i18n';
import {scale} from '../libs/reactSizeMatter/scalingUtils';
import {CommonColors, Fonts} from '../utils/CommonStyles';

class LabelComponent extends Component {
    render() {
        const {focused, title} = this.props;
        return (
            <Text
                allowFontScaling={false}
                style={{
                    color: focused ? CommonColors.activeTintColor : CommonColors.secondaryColor,
                    fontSize: scale(11),
                    ...Fonts.defaultRegular,
                    textAlign: 'center',
                    marginBottom: scale(5),
                }}
            >
                {I18n.t(title)}
            </Text>
        );
    }
}

const styles = ScaledSheet.create({
    container: {
        flex: 1,
    },
});

const mapStateToProps = state => ({
    user: state.user,
});

export default connect(mapStateToProps)(LabelComponent);
