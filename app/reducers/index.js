import {combineReducers} from 'redux';
import loadingReducer from './loadingReducer';
import colorsReducer from './colorsReducer';
import masterDataReducer from './masterdataReducer';
import bodyTypeReducer from './bodyTypeReducer';
import userInformationReducer from './userInformationReducer';
import regionReducer from './regionReducer';

export default combineReducers({
    loading: loadingReducer,
    colors: colorsReducer,
    bodyType: bodyTypeReducer,
    masterData: masterDataReducer,
    user: userInformationReducer,
    region: regionReducer,
});
