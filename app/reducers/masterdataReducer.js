import {FETCH_MASTER_DATA_SUCCESS} from '../actions/types';

const initialState = [];

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_MASTER_DATA_SUCCESS:
            return action.payload;
        default:
            return state;
    }
}
