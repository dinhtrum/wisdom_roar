import {FETCH_COLORS_SUCCESS} from '../actions/types';

const initialState = [];

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_COLORS_SUCCESS:
            return action.payload;
        default:
            return state;
    }
}
