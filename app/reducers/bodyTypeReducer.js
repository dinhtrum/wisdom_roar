import {FETCH_BODY_TYPES_SUCCESS} from '../actions/types';

const initialState = {
    body_type: [],
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_BODY_TYPES_SUCCESS:
            return {
                body_type: action.payload,
            };
        default:
            return state;
    }
}
