import {CANCEL_FETCH_REGIONS, FETCH_REGIONS_SUCCESS} from '../actions/types';

const initialState = [];

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REGIONS_SUCCESS:
            return action.payload;
        case CANCEL_FETCH_REGIONS:
            return [];
        default:
            return state;
    }
}
