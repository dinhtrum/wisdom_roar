import { StatusBar, Platform } from 'react-native';
import { moderateScale, scale } from '../libs/reactSizeMatter/scalingUtils';
import Utils from './Utils';

const Fonts = {
  dinPro: {
    fontFamily: 'DINPro-Medium',
  },
  defaultThin: {
    fontFamily: 'Roboto-Thin',
  },
  defaultRegular: {
    fontFamily: 'Montserrat-Regular',
  },
  defaultMedium: {
    fontFamily: 'Roboto-Medium',
  },
  defaultBold: {
    fontFamily: 'Montserrat-SemiBold',
  },
  defaultBlack: {
    fontFamily: 'Roboto-Black',
  },
};

class CommonColors {
  static mainColor = '#FF9500';

  static secondaryColor = '#4F9DA6';

  static screenBgColor = '#FF9500';

  static increased = '#2dac91';

  static decreased = '#f74940';

  static activeTintColor = '#FFAD5A';

  static inActiveTintColor = '#383b6b';

  static border = '#D1D1D6';

  static separator = '#BBBBBB';

  static mainText = '#FFFFFF';

  static secondaryText = '#000000';

  static headerBarBgColor = '#FF9500';

  static headerTitleColor = '#FFFFFF';

  static disableText = '#444774';

  static placeholder = '#444774';

  static popupBorder = '#E5E5E5';


  static popupBg = '#ffffff';

  static darkTextColor = '#666666';

  static lightTextColor = '#FFF';

  static btnSubmitBgColor = '#1aa4fa';

  static listItemBgColor = '#F8F8F8';

  static textInputBgColor = '#1a2030';

  static inputTitleColor = 'white';

  static inputSearchBox = '#1b1d2b';

  static modalBackdropAlpha = 0.4;

  static modalColor = '#e8ba00';

  static buttonTextColor = '#FFFFFF';

  static noticeColor = '#f74940';

  static black = '#000000';

  static thirdColor = '#FF5959'
}

class CommonSize {
  static contentPadding = scale(16);

  static headerTitleFontSize = '15@ms';

  static inputHeight = '40@s';

  static inputFontSize = '14@ms';

  static formLabelFontSize = '14@ms';

  static btnSubmitHeight = scale(35);

  static paddingTopHeader = Platform.OS === 'ios'
    ? (Utils.isIphoneX() ? scale(34) : scale(20)) : StatusBar.currentHeight;

  static headerHeight = scale(44) + CommonSize.paddingTopHeader;

  static marginBottom = scale(30)
}

const CommonStyles = {
  screen: {
    flex: 1,
    // backgroundColor: CommonColors.screenBgColor,
  },
  header: {
    backgroundColor: CommonColors.headerBarBgColor,
    elevation: 0,
    height: CommonSize.headerHeight,
    paddingTop: CommonSize.paddingTopHeader,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerHome: {
    backgroundColor: CommonColors.headerBarBgColor,
    elevation: 0,
    height: CommonSize.headerHeight + scale(13),
    paddingTop: CommonSize.paddingTopHeader,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerTitle: {
    fontSize: moderateScale(18),
    fontWeight: '500',
    color: CommonColors.headerTitleColor,
    textAlignVertical: 'center',
    textAlign: 'center',
    ...Fonts.defaultRegular,
  },
  priceIncreased: {
    color: CommonColors.increased,
  },
  priceDecreased: {
    color: CommonColors.decreased,
  },
  priceNotChanged: {
    color: CommonColors.mainText,
  },
  matchParent: {
    flex: 1,
  },
  inputFocused: {
    borderColor: CommonColors.activeTintColor,
    borderWidth: scale(1),
  },
  buttonSubmitDisabled: {
    backgroundColor: CommonColors.inActiveTintColor,
    height: CommonSize.btnSubmitHeight,
    width: '100%',
    marginTop: scale(20),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: scale(3),
  },
  buttonSubmit: {
    backgroundColor: CommonColors.activeTintColor,
    height: CommonSize.btnSubmitHeight,
    width: '100%',
    marginTop: scale(20),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: scale(3),
  },
  textButtonSubmit: {
    fontSize: scale(13),
    ...Fonts.defaultBold,
    textTransform: 'uppercase',
    color: CommonColors.lightTextColor,
    letterSpacing: scale(1),
  },
  iconToolbar: {
    width: scale(50),
    height: scale(50),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  errorMessage: {
    fontSize: scale(11),
    textAlign: 'left',
    color: 'red',
  },
  separatorStyle: {
    height: scale(1),
    backgroundColor: CommonColors.separator,
  },
};

const ShadowStyle = {
  shadowColor: '#000',
  shadowOpacity: 0.2,
  shadowOffset: {
    width: 0,
    height: 8,
  },
  elevation: 3,
};

const TextButtonStyle = {
  fontSize: '12@s',
  fontWeight: '500',
  textTransform: 'uppercase',
  color: CommonColors.mainText,
};

const SeparatorStyle = {
  width: '100%',
  height: scale(1),
  backgroundColor: CommonColors.separator,
};


export {
  CommonStyles, CommonColors, CommonSize, Fonts, ShadowStyle, TextButtonStyle, SeparatorStyle
};
