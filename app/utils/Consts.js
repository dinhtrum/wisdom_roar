import I18n from '../i18n/i18n';

export default class Consts {

    static CATEGORY = [
        {
            title: 'Sách văn học',
            illustration: 'https://wallpaperplay.com/walls/full/4/f/0/162413.jpg',
        },
        {
            title: 'Sách Công Nghệ Thông Tin',
            illustration: 'https://ak0.picdn.net/shutterstock/videos/17474440/thumb/12.jpg',
        },
        {
            title: 'Sách kinh tế',
            illustration: 'https://www.york.ac.uk/media/study/courses/postgraduate/economics/econometrics-economics-hero-credit-istock-1160.jpg',
        },
        {
            title: 'Sách kỹ năng sống',
            illustration: 'https://i.imgur.com/MABUbpDl.jpg',
        },
        {
            title: 'Sách Bà mẹ - Em bé',
            illustration: 'https://i.imgur.com/2nCt3Sbl.jpg',
        },
        {
            title: 'Sách Giáo Khoa - Giáo Trình',
            illustration: 'https://i.imgur.com/lceHsT6l.jpg',
        },
        {
            title: 'Sách Học Ngoại Ngữ',
            illustration: 'https://i.imgur.com/KZsmUi2l.jpg',
        },
        {
            title: 'Sách Tham Khảo',
            illustration: 'https://cdn3.vectorstock.com/i/1000x1000/18/37/beautiful-background-with-flora-vector-781837.jpg',
        },
        {
            title: 'Từ Điển',
            illustration: 'https://as2.ftcdn.net/jpg/02/05/31/43/500_F_205314374_ZUeFTO8hWcrvUhDCYxh6aw464ScAOC5n.jpg',
        },
        {
            title: 'Sách Kiến Thức Tổng Hợp',
            illustration: 'https://i.imgur.com/KZsmUi2l.jpg',
        },
        {
            title: 'Sách Khoa Học - Kỹ Thuật',
            illustration: 'https://previews.123rf.com/images/subbotina/subbotina1907/subbotina190700018/126609017-autumn-colorful-bright-leaves-swinging-on-an-oak-tree-in-autumnal-park-fall-background-beautiful-nat.jpg',
        },
        {
            title: 'Sách Lịch sử',
            illustration: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdT2FzoNUSAcDmp_h53Bxn3nhKEUhPe-Zm_KDhEfwAZPsjaQEP&s',
        },
        {
            title: 'Sách thiếu nhi',
            illustration: 'https://png.pngtree.com/thumb_back/fw800/back_our/20190621/ourmid/pngtree-4-2-international-children-s-book-day-beautiful-cartoon-reading-girl-poster-image_202876.jpg',
        },
        {
            title: 'Điện Ảnh - Nhạc - Họa',
            illustration: 'https://i.imgur.com/UYiroysl.jpg',
        },
        {
            title: 'Truyện Tranh, Manga, Comic',
            illustration: 'https://i.imgur.com/KZsmUi2l.jpg',
        },
        {
            title: 'Sách Tôn Giáo - Tâm Linh',
            illustration: 'https://s3.envato.com/files/266724992/Morning_dew_17.jpg',
        },
        {
            title: 'Sách Văn Hóa - Địa Lý - Du Lịch',
            illustration: 'https://image.freepik.com/free-photo/beautiful-background-roses-valentine-s-day_24972-167.jpg',
        },
        {
            title: 'Sách Chính Trị - Pháp Lý',
            illustration: 'https://i.ytimg.com/vi/X42N5384rLk/maxresdefault.jpg',
        },
        {
            title: 'Sách Nông - Lâm - Ngư Nghiệp',
            illustration: 'https://i.imgur.com/UYiroysl.jpg',
        },
        {
            title: 'Sách Y Học',
            illustration: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSj1fv1aGNRKnLRhDKsd2f86_AVKN5T22vKZXjoN-DIKyBpLRXfcA&s',
        },
        {
            title: 'Tạp Chí - Catalogue',
            illustration: 'https://ae01.alicdn.com/kf/HTB1fRo0JpXXXXblXVXXq6xXFXXXZ/1x1-5m-Cool-Backdrops-Scenic-Beautiful-Scenery-Family-Art-Portaits-Theme-Photoaphy-Background-Vinyl-Free-Shipping.jpg',
        },
        {
            title: 'Sách Tâm lý - Giới tính',
            illustration: 'https://ak3.picdn.net/shutterstock/videos/13442093/thumb/1.jpg',
        },
        {
            title: 'Sách Thường Thức - Gia Đình',
            illustration: 'https://thumbs.dreamstime.com/z/beautiful-background-words-family-mother-s-day-father-s-day-idea-postcards-65338303.jpg',
        },
    ];

}
