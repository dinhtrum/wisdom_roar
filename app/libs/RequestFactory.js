import UserRequest from '../requests/UserRequest';
import HomeRequest from '../requests/HomeRequest';
import CarRequest from '../requests/CarRequest';
import ShopRequest from '../requests/ShopRequest';

const requestMap = {
  UserRequest,
  HomeRequest,
  CarRequest,
  ShopRequest
};

const instances = {};

export default class RequestFactory {
  static getRequest(classname) {
    const RequestClass = requestMap[classname];
    if (!RequestClass) {
      throw new Error(`Invalid request class name: ${classname}`);
    }

    let requestInstance = instances[classname];
    if (!requestInstance) {
      requestInstance = new RequestClass();
      instances[classname] = requestInstance;
    }

    return requestInstance;
  }
}
