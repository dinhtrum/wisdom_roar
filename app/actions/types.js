export const SHOW_LOADING = 'show_loading';
export const HIDE_LOADING = 'hide_loading';
export const FETCH_COLORS = 'fetch_colors';
export const FETCH_COLORS_SUCCESS = 'fetch_colors_success';
export const CANCEL_FETCH_COLORS = 'cancel_fetch_colors';
export const FETCH_MASTER_DATA = 'fetch_master_data';
export const FETCH_MASTER_DATA_SUCCESS = 'fetch_master_data_success';
export const CANCEL_FETCH_MASTER_DATA = 'cancel_fetch_master_data';
export const FETCH_BODY_TYPES = 'fetch_body_types';
export const FETCH_BODY_TYPES_SUCCESS = 'fetch_body_types_success';
export const CANCEL_FETCH_BODY_TYPES = 'cancel_fetch_body_types';

export const FETCH_USER_INFORMATION = 'fetch_user_information';
export const UPDATE_USER_INFORMATION = 'update_user_information';
export const FETCH_USER_INFORMATION_SUCCESS = 'fetch_user_information_success';
export const UPDATE_USER_INFORMATION_SUCCESS = 'update_user_information_success';
export const UPDATE_LOGIN_STATUS = 'update_login_status';
export const UPDATE_LANGUAGE = 'update_language';
export const CHANGE_LANGUAGE = 'change_language';
export const LOG_OUT = 'log_out';

export const FETCH_REGIONS = 'fetch_regions';
export const FETCH_REGIONS_SUCCESS = 'fetch_regions_success';
export const CANCEL_FETCH_REGIONS = 'cancel_fetch_regions';



