import * as actionTypes from './types';

export const fetchColors = () => ({ type: actionTypes.FETCH_COLORS });
export const fetchMasterData = () => ({ type: actionTypes.FETCH_MASTER_DATA });
export const fetchBodyTypes = () => ({ type: actionTypes.FETCH_BODY_TYPES });

export const showLoading = () => ({ type: actionTypes.SHOW_LOADING });

export const hideLoading = () => ({ type: actionTypes.HIDE_LOADING });
export const fetchUserInformation = () => ({ type: actionTypes.FETCH_USER_INFORMATION });
export const updateUserInformation = () => ({ type: actionTypes.UPDATE_USER_INFORMATION });
export const changeLanguage = params => ({ type: actionTypes.CHANGE_LANGUAGE, params });

export const logOut = () => ({ type: actionTypes.LOG_OUT });

export const fetchRegions = () => ({ type: actionTypes.FETCH_REGIONS });


