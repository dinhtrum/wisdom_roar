import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  ImageBackground,
  StatusBar,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
  ActivityIndicator,
  Alert,
} from 'react-native';
import ScaledSheet from '../../libs/reactSizeMatter/ScaledSheet';
import Text from '../../component/Text';
import RevealIcon from '../../../assets/svg/reveal.svg';
import RevealActiveIcon from '../../../assets/svg/reveal_active.svg';
import LogoD2 from '../../../assets/svg/app/logo.svg';
import FloatingLabelInputError from '../../component/FloatingLabelInputError';
import I18n from '../../i18n/i18n';
import DialogUtil from '../../utils/DialogUtil';
import rf from '../../libs/RequestFactory';
import AppPreferences from '../../utils/AppPreferences';
import * as actions from '../../actions';
import store from '../../store';
import {SvgXml} from 'react-native-svg';
import Navigator from '../../utils/Navigator';
import {moderateScale, scale} from '../../libs/reactSizeMatter/scalingUtils';
import {CommonColors, ShadowStyle} from '../../utils/CommonStyles';
import FloatingLabelInput from '../../component/FloatingLabelInput';
import Header from '../../component/Header';
import BackButton from '../../component/BackButton';
import {connect} from 'react-redux';
import {showLoading} from '../../actions';
import {hideLoading} from '../../actions';
import VccLoading from '../../component/VccLoading';
import _ from 'lodash';

class LoginScreen extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      username: 'dinhnt',
      password: '917381',
      revealPassword: true,
      showLoading: false,
    };
  }

  render() {
    const {revealPassword, username, showLoading} = this.state;
    const size = Platform.OS === 'ios' ? 'large' : scale(40);
    return (
      <ImageBackground
        resizeMode={'stretch'}
        source={require('../../../assets/images/bg.png')}
        style={styles.container}>
        <Header
          headerStyle={{backgroundColor: 'transparent'}}
          // left={<BackButton isLogin={true}/>}
        />
        <StatusBar
          backgroundColor="transparent"
          translucent
          barStyle="dark-content"
        />
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View style={{marginHorizontal: scale(16)}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                marginBottom: scale(35),
              }}>
              <Text style={styles.loginTitle}>Login</Text>
              <SvgXml xml={LogoD2} />
            </View>

            <FloatingLabelInput
              label={I18n.t('app.user_name')}
              placeholder={I18n.t('app.your_user_name')}
              value={username}
              onChangeText={username => this.setState({username})}
            />

            <View>
              <FloatingLabelInput
                label={I18n.t('app.password')}
                placeholder={I18n.t('app.your_password')}
                secureTextEntry={revealPassword}
                containerStyle={{marginTop: scale(10)}}
                value={this.state.password}
                onChangeText={password => this.setState({password})}
              />
              {this.state.password.trim() !== '' && (
                <TouchableOpacity
                  style={{position: 'absolute', bottom: scale(8), right: 10}}
                  hitSlop={{top: 20, bottom: 20, left: 20, right: 20}}
                  onPress={() => this.showPassword()}>
                  {revealPassword && <SvgXml xml={RevealIcon} />}
                  {!revealPassword && <SvgXml xml={RevealActiveIcon} />}
                </TouchableOpacity>
              )}
            </View>
            {this.renderLoginButton()}
            <Text
              onPress={() => Navigator.navigate('ForgotPasswordScreen')}
              style={styles.forgotPassword}>
              {I18n.t('app.forgot_password')}?
            </Text>
          </View>
        </TouchableWithoutFeedback>
        {showLoading && (
          <ActivityIndicator
            animating
            size={size}
            color={CommonColors.mainColor}
          />
        )}
      </ImageBackground>
    );
  }

  renderLoginButton = () => {
    const {username, password, showLoading} = this.state;
    const enableButton =
      username && username.trim() !== '' && password && password.trim() !== '';
    const opacity = enableButton ? 1 : 0.4;
    return (
      <TouchableOpacity
        disabled={!enableButton || showLoading}
        onPress={() => this.onClickLogin()}
        style={[styles.loginButton, {opacity}]}>
        <Text style={styles.loginLabel}>{I18n.t('app.login')}</Text>
      </TouchableOpacity>
    );
  };

  onClickLogin = () => {
    Keyboard.dismiss();
    this.handleLogin();
  };

  handleLogin = async () => {
    const {username, password} = this.state;
    this.setState({
      showLoading: true,
    });
    try {
      const res = await rf.getRequest('UserRequest').login(username, password);
      this.setState({
        showLoading: false,
      });
      this._loginSuccess(username, res);
    } catch (error) {
      this.setState({
        showLoading: false,
      });
      // DialogUtil.showMessageDialog('Username or password incorrect.');
      Alert.alert(I18n.t('app.Alert'), I18n.t('app.account_incorrect'), [
        {text: 'OK', onPress: () => {}},
      ]);
    }
  };

  async _loginSuccess(responseUser, account) {
    await AppPreferences.saveAccessToken(responseUser);
    await AppPreferences.saveAccountLogin(JSON.stringify(account));
    store.dispatch(actions.fetchUserInformation());
    Navigator.navigate('HomeTab');
  }

  showPassword = () => {
    this.setState({revealPassword: !this.state.revealPassword});
  };
}

const styles = ScaledSheet.create({
  container: {
    width: '100%',
    height: '100%',
  },
  forgotPassword: {
    fontSize: '12@ms',
    textDecorationLine: 'underline',
    alignSelf: 'center',
    marginVertical: '30@s',
  },
  loginButton: {
    width: '100%',
    height: '40@s',
    backgroundColor: CommonColors.mainColor,
    borderRadius: '20@s',
    flexDirection: 'row',
    alignItems: 'center',
    ...ShadowStyle,
    marginTop: scale(50),
  },
  loginLabel: {
    color: '#fff',
    fontSize: moderateScale(14),
    fontWeight: '500',
    flex: 1,
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  loginTitle: {
    fontSize: '34@ms',
    fontWeight: 'bold',
    color: '#000',
  },
});

const mapStateToProps = state => ({
  isLogin: state.user.isLogin,
});

const mapDispatchToProps = dispatch => ({
  showLoading: () => {
    dispatch(showLoading());
  },
  hideLoading: () => {
    dispatch(hideLoading());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginScreen);
