import React, {Component} from 'react';
import {View, TouchableOpacity, Image} from 'react-native';
import ScaledSheet from '../libs/reactSizeMatter/ScaledSheet';
import {CommonColors, Fonts, ShadowStyle} from '../utils/CommonStyles';
import {scale, verticalScale} from '../libs/reactSizeMatter/scalingUtils';
import ImageCommon from '../component/Image';
import {hideLoading, showLoading} from '../actions';
import Navigator from '../utils/Navigator';
import DialogUtil from '../utils/DialogUtil';
import {connect} from 'react-redux';
import {SvgXml} from 'react-native-svg';
import StarIcon from '../../assets/svg/app/star.svg';
import Text from '../component/Text';
import {getTimeDay, getTimeStamp} from '../utils/Filters';
import _ from 'lodash';
import {NavigationActions, StackActions} from 'react-navigation';
import Utils from '../utils/Utils';
import I18n from '../i18n/i18n'

class BorrowBookScreen extends Component {

    componentDidMount() {
        Utils.showSuccessToast({ message: I18n.t('app.borrow_book_successfully'), position: Utils.TOAST_POSITION_TOP });
    }

    render() {
        const data = _.get(this.props.navigation.state.params, 'data', {});
        const book = _.get(this.props.navigation.state.params, 'book', {});
        const thumbnail = _.get(book, 'thumbnail', null);
        const main_title = _.get(book, 'main_title', '');
        const author = _.get(book, 'author', '');
        const username = _.get(this.props.user, 'display_name', '');
        const borrow_date = _.get(data, 'borrow_date', new Date());
        const return_date = _.get(data, 'return_date', new Date());
        const image = thumbnail ? thumbnail.split(',')[0] : null;
        const category = _.get(book, 'category', '');

        return (
            <View style={styles.container}>
                <Image
                    resizeMode={'cover'}
                    style={styles.headerImageBook}
                    source={require('../../assets/images/header_book.png')}/>
                <View style={styles.content}>
                    <View style={styles.bookAvatarView}>
                        <ImageCommon
                            resizeMode={'cover'}
                            uri={image}
                            style={styles.bookAvatar}/>
                    </View>
                    <Text style={styles.category}>{category}</Text>
                    <Text numberOfLines={3} style={styles.bookName}>{main_title}</Text>
                    <View style={styles.authorView}>
                        <Text style={styles.authorName}>{author}</Text>
                        <Text style={styles.separatorColumn}>/</Text>
                        <SvgXml xml={StarIcon}/>
                        <Text style={styles.starNumber}>5.0</Text>
                    </View>
                    <View style={styles.separator}/>
                    <View style={{marginBottom: verticalScale(20)}}>
                        <Text style={styles.borrowerName}>{I18n.t('app.borrower_name')}</Text>
                        <Text style={styles.valueLabel}>{username}</Text>
                    </View>
                    {/*<View style={{marginBottom: verticalScale(20)}}>*/}
                    {/*    <Text style={styles.borrowerName}>Borrower email</Text>*/}
                    {/*    <Text style={styles.valueLabel}>johndoe@vnext.com.vn</Text>*/}
                    {/*</View>*/}
                    <View style={styles.borrowTime}>
                        <View style={{flex: 1, marginRight: scale(30)}}>
                            <Text style={styles.borrowerName}>Borrow time</Text>
                            <Text style={styles.valueLabel}>{getTimeDay(borrow_date, 'HH : mm : ss')}</Text>
                            <Text style={styles.valueLabel}>{getTimeDay(borrow_date, 'DD / MM /YYYY')}</Text>
                        </View>
                        <View style={{flex: 1}}>
                            <Text style={styles.borrowerName}>{I18n.t('app.deadline_return')}</Text>
                            <Text style={styles.valueLabel}>{getTimeDay(return_date, 'HH:mm:ss')}</Text>
                            <Text
                                style={styles.valueLabel}>{getTimeDay(return_date, 'DD / MM /YYYY')}</Text>
                        </View>
                    </View>
                    <View style={{flex: 1}}/>
                    <TouchableOpacity
                        onPress={() => this._onClickDone()}
                        style={styles.button}>
                        <Text style={styles.buttonLabel}>{I18n.t('app.back_to_home')}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    _onClickDone = () => {
       Navigator.resetScreen('Home')
    };
}

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        backgroundColor: CommonColors.screenBgColor,
    },
    content: {
        width: '359@s',
        marginHorizontal: '8@s',
        backgroundColor: '#fff',
        flex: 1,
        ...ShadowStyle,
        borderTopLeftRadius: '20@s',
        borderTopRightRadius: '20@s',
        marginTop: '140@vs',
        padding: '16@s',
    },
    bookAvatar: {
        width: '100@vs',
        height: '125@vs',
        backgroundColor: '#AAD6FF',
        borderRadius: '3@s',
    },
    bookName: {
        fontSize: '18@ms',
        color: '#000',
        alignSelf: 'center',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    authorName: {
        fontSize: '13@ms',
        color: '#8E8E93',
        fontWeight: '600',
        ...Fonts.defaultBold,
    },
    separatorColumn: {
        fontSize: '13@ms',
        color: '#000',
        marginHorizontal: '14@s',
    },
    separator: {
        backgroundColor: '#339DFF',
        width: '150@s',
        height: '3@s',
        alignSelf: 'center',
        marginTop: '20@vs',
        marginBottom: '15@vs',
    },
    borrowerName: {
        fontSize: '15@ms',
        fontWeight: '600',
        color: '#000000',
        marginBottom: '10@s',
    },
    valueLabel: {
        fontSize: '15@ms',
        fontWeight: 'normal',
        color: '#000000',
    },
    borrowTime: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    buttonLabel: {
        fontSize: '16@ms',
        color: '#fff',
        fontWeight: '500',
    },
    button: {
        width: '100%',
        height: '40@s',
        backgroundColor: '#339DFF',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: '20@s',
        marginBottom: '30@s',
        marginTop: '20@s',
        ...ShadowStyle,
    },
    headerImageBook: {
        width: '100%',
        height: '335@s',
        position: 'absolute',
    },
    category: {
        paddingHorizontal: '10@s',
        paddingVertical: '5@s',
        backgroundColor: 'rgba(255,149,0, 0.25)',
        color: '#FF9500',
        marginTop: '70@vs',
        borderRadius: '3@s',
        marginBottom: '10@vs',
        alignSelf: 'center',
        fontSize: '11@ms',
    },
    bookAvatarView: {
        position: 'absolute',
        top: -verticalScale(60.5),
        alignSelf: 'center',
        shadowColor: '#FF9500',
        shadowOpacity: 0.2,
        shadowOffset: {
            width: 2,
            height: 6,
        },
        elevation: 3,
    },
    starNumber: {
        fontSize: '13@ms',
        color: '#000',
        fontWeight: '600',
        marginLeft: '5@s',
    },
    authorView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: '17@vs',
        alignSelf: 'center',
    },
});

const mapStateToProps = state => ({
    user: state.user.profile,
});

const mapDispatchToProps = dispatch => ({
    showLoading: () => {
        dispatch(showLoading());
    },
    hideLoading: () => {
        dispatch(hideLoading());
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(BorrowBookScreen);
