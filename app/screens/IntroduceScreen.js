import React, {Component} from 'react';
import {Image, View, TouchableOpacity} from 'react-native';
import ScaledSheet from '../libs/reactSizeMatter/ScaledSheet';
import {scale} from '../libs/reactSizeMatter/scalingUtils';
import Text from '../component/Text';
import Swiper from 'react-native-swiper';
import I18n from '../i18n/i18n';

const data = [
    {
        step: 'app.step_1',
        title: 'app.step_1_title',
        content: 'app.step_1_content',
    },
    {
        step: 'app.step_2',
        title: 'app.step_2_title',
        content: 'app.step_2_content',
    },
    {
        step: 'app.step_3',
        title: 'app.step_3_title',
        content: 'app.step_3_content',
    },
    {
        step: 'app.step_4',
        title: 'app.step_4_title',
        content: 'app.step_4_content',
    },

];

export default class IntroduceScreen extends Component {

    render() {
        return (
            <View style={{flex:1}}>
                <View style={{width: '100%', flex:1, marginTop: scale(50)}}>
                    <Swiper
                        autoplay={true}
                        activeDot={this.renderActiveDot()}
                        paginationStyle={{bottom: -scale(20)}}
                        showsButtons={false}>
                        {data.map((item, index) => (
                            <View key={index} style={{flex: 1, height: scale(214), marginHorizontal: scale(16)}}>
                                <View style={styles.header}>
                                    <Text style={styles.stepLabel}>{I18n.t(item.step)}</Text>
                                </View>
                                <View style={styles.content}>
                                    <View style={{flex: 1, padding: scale(16)}}>
                                        <Text style={styles.title}>{I18n.t(item.title)}</Text>
                                        <Text style={styles.message}>{I18n.t(item.content)}</Text>
                                    </View>
                                    <View style={styles.viewMoreButton}>
                                        <Text style={styles.viewMoreLabel}>{I18n.t('app.view_more')}</Text>
                                    </View>
                                </View>
                            </View>
                        ))}
                    </Swiper>
                </View>
                <TouchableOpacity activeOpacity={0.7} style={styles.loginButton}>
                    <Text style={styles.loginLabel}>Log in with email VNEXT</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Text style={styles.notNow}>Not now</Text>
                </TouchableOpacity>
            </View>
        );
    }

    renderActiveDot = () => {
        return (
            <View style={{
                backgroundColor: '#FFAD5A',
                width: 16,
                height: 8,
                borderRadius: 8,
                marginLeft: 3,
                marginRight: 3,
                marginTop: 3,
                marginBottom: 3,
            }}/>
        );
    };
}

const styles = ScaledSheet.create({
    header: {
        width: '100%',
        height: scale(40),
        backgroundColor: '#FFC183',
        borderTopLeftRadius: '3@s',
        borderTopRightRadius: '3@s',
        justifyContent: 'center',
    },
    stepLabel: {
        fontSize: '18@ms',
        fontWeight: '500',
        textTransform: 'uppercase',
        color: '#fff',
        marginLeft: '16@s',
    },
    content: {
        flex: 1,
        borderBottomLeftRadius: '3@s',
        borderBottomRightRadius: '3@s',
        borderWidth: scale(1),
        borderColor: '#FFAD5A',
        overflow: 'hidden',
    },
    title: {
        fontSize: '16@ms',
        fontWeight: '500',
        color: '#333333',
        marginBottom: '16@s',
    },
    message: {
        fontSize: '16@ms',
        fontWeight: 'normal',
        color: '#333333',
        lineHeight: '24@ms',
    },
    viewMoreButton: {
        width: '100%',
        height: '32@s',
        backgroundColor: '#4F9DA6',
        alignItems: 'center',
        justifyContent: 'center',
    },
    viewMoreLabel: {
        fontSize: '12@ms',
        fontWeight: '500',
        color: '#FFFFFF',
        textTransform: 'uppercase',
    },
    loginButton:{
        width: '335@s',
        height: '40@s',
        borderRadius: '20@s',
        backgroundColor: '#339DFF',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf:'center',
        marginVertical: '30@s'
    },
    loginLabel: {
        fontSize: '15@ms',
        color: '#fff'
    },
    notNow:{
        fontSize: '22@ms',
        color: '#000',
        alignSelf: 'center',
        marginBottom: '50@s'
    }
});
