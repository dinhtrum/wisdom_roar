import React, {Component} from 'react';
import {View, Image, TouchableWithoutFeedback, TouchableOpacity, Keyboard} from 'react-native';
import ScaledSheet from '../../libs/reactSizeMatter/ScaledSheet';
import Text from '../../component/Text';
import ImageCommon from '../../component/Image';
import {CommonColors, CommonStyles, ShadowStyle} from '../../utils/CommonStyles';
import RevealIcon from '../../../assets/svg/reveal.svg';
import RevealActiveIcon from '../../../assets/svg/reveal_active.svg';
import {SvgXml} from 'react-native-svg';
import Navigator from '../../utils/Navigator';
import FloatingLabelInput from '../../component/FloatingLabelInput';
import DialogUtil from '../../utils/DialogUtil';
import rf from '../../libs/RequestFactory';
import {hideLoading, showLoading} from '../../actions';
import {connect} from 'react-redux';
import {scale} from '../../libs/reactSizeMatter/scalingUtils';
import Header from '../../component/Header';
import I18n from '../../i18n/i18n';
import BackButton from '../../component/BackButton';

export default class ChangePasswordScreen extends Component {

    state = {
        currentPassword: '',
        newPassword: '',
        confirmPassword: '',
        revealCurrentPassword: true,
        revealNewPassword: true,
        revealConfirmPassword: true,
    };

    render() {
        const {
            currentPassword, newPassword, confirmPassword, revealNewPassword,
            revealConfirmPassword, revealCurrentPassword,
        } = this.state;
        return (
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Image style={styles.headerImage}
                               resizeMode={'cover'}
                               source={require('../../../assets/images/header_user.png')}/>
                        <View style={styles.avatarView}>
                            <View style={{...ShadowStyle}}>
                                <ImageCommon
                                    resizeMode={'cover'}
                                    style={styles.avatar}
                                    uri={'https://tophinhnen.com/wp-content/uploads/2018/03/hinh-nen-anime-girl-dang-yeu-7.jpg'}/>
                            </View>

                            <View style={styles.userName}>
                                <Text style={styles.name}>JOHN DOE</Text>
                                <Text>20 / 04 / 1995</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{paddingHorizontal: scale(16)}}>
                        <View>
                            <FloatingLabelInput label="Current password *"
                                                secureTextEntry={revealCurrentPassword}
                                                value={currentPassword}
                                                onChangeText={(currentPassword) => this.setState({currentPassword})}/>
                            {
                                currentPassword.trim() !== '' && <TouchableOpacity
                                    style={styles.reveal}
                                    hitSlop={{top: 20, bottom: 20, left: 20, right: 20}}
                                    onPress={() => this.showCurrentPassword()}>
                                    {revealCurrentPassword && <SvgXml xml={RevealIcon}/>}
                                    {!revealCurrentPassword && <SvgXml xml={RevealActiveIcon}/>}
                                </TouchableOpacity>
                            }
                        </View>
                        <View>
                            <FloatingLabelInput
                                label="New password *"
                                secureTextEntry={revealNewPassword}
                                value={this.state.newPassword}
                                onChangeText={(newPassword) => this.setState({newPassword})}/>
                            {
                                newPassword.trim() !== '' && <TouchableOpacity
                                    style={styles.reveal}
                                    hitSlop={{top: 20, bottom: 20, left: 20, right: 20}}
                                    onPress={() => this.showNewPassword()}>
                                    {revealNewPassword && <SvgXml xml={RevealIcon}/>}
                                    {!revealNewPassword && <SvgXml xml={RevealActiveIcon}/>}
                                </TouchableOpacity>
                            }
                        </View>
                        <View style={{marginTop: scale(15)}}>
                            <FloatingLabelInput
                                label="Confirm password *"
                                secureTextEntry={revealConfirmPassword}
                                value={confirmPassword}
                                onChangeText={(confirmPassword) => this.setState({confirmPassword})}/>
                            {
                                this.state.confirmPassword.trim() !== '' && <TouchableOpacity
                                    style={styles.reveal}
                                    hitSlop={{top: 20, bottom: 20, left: 20, right: 20}}
                                    onPress={() => this.showConfirmPassword()}>
                                    {revealConfirmPassword && <SvgXml xml={RevealIcon}/>}
                                    {!revealConfirmPassword && <SvgXml xml={RevealActiveIcon}/>}
                                </TouchableOpacity>
                            }
                        </View>
                        {this.renderChangePasswordButton()}
                    </View>
                    {this.renderHeader()}
                </View>
            </TouchableWithoutFeedback>
        );
    }

    renderHeader = () => {
        return (
            <Header
                headerStyle={{position: 'absolute', width: '100%', backgroundColor: 'transparent'}}
                left={this.renderLeftHeader()}
                center={this.renderCenterHeader()}
                // right={this.renderRightHeader()}
            />
        );
    };

    renderLeftHeader = () => <BackButton/>;

    renderCenterHeader = () => (
        <Text style={CommonStyles.headerTitle}>{I18n.t('app.change_password')}</Text>
    );

    showCurrentPassword = () => {
        this.setState({revealCurrentPassword: !this.state.revealCurrentPassword});
    };

    showNewPassword = () => {
        this.setState({revealNewPassword: !this.state.revealNewPassword});
    };

    showConfirmPassword = () => {
        this.setState({revealConfirmPassword: !this.state.revealConfirmPassword});
    };

    renderChangePasswordButton = () => {
        const {currentPassword, newPassword, confirmPassword} = this.state;
        const enableButton = currentPassword && currentPassword.trim() !== '' && newPassword && newPassword.trim() !== ''
            && confirmPassword && confirmPassword.trim() !== '';
        const opacity = enableButton ? 1 : 0.2;
        return (
            <TouchableOpacity
                disabled={!enableButton}
                onPress={() => this.onClickChangePassword()}
                activeOpacity={0.7}
                style={[styles.button, {opacity}]}>
                <Text style={styles.buttonLabel}>Save</Text>
            </TouchableOpacity>
        );
    };

    onClickChangePassword = async () => {
        Keyboard.dismiss();
        const {currentPassword, newPassword, confirmPassword} = this.state;
        if (confirmPassword !== newPassword) {
            alert('The confirm password does not match.', () => {});
        } else {
            const {username} = this.props.user.profile;
            this.props.showLoading();
            try {
                const res = await rf.getRequest('UserRequest').changePassword(currentPassword, newPassword, confirmPassword, username);
                this.props.hideLoading();
                Navigator.goBack();
            } catch (error) {
                this.props.hideLoading();
                if (error.message === 'password.incorrect') {
                    alert('Current password incorrect');
                } else {
                    alert(error.message);
                }
            }
        }
    };
}

const styles = ScaledSheet.create({
    container: {
        flex: 1,
    },
    header: {
        height: '200@s',
    },
    headerImage: {
        width: '100%',
        height: '140@s',
    },
    avatar: {
        width: '100@s',
        height: '100@s',
        borderRadius: '50@s',
        borderWidth: '5@s',
        borderColor: '#FFCC00',
    },
    avatarView: {
        flexDirection: 'row',
        position: 'absolute',
        left: '20@s',
        bottom: '20@s',
    },
    userName: {
        height: '100@s',
        flex: 1,
        justifyContent: 'center',
        paddingVertical: '20@s',
        marginLeft: '10@s',
    },
    name: {
        color: '#fff',
        fontSize: '17@ms',
        fontWeight: '600',
        marginBottom: '20@s',
    },
    title: {
        fontSize: '15@ms',
        fontWeight: '500',
        color: '#000000',
        flex: 1,
        marginHorizontal: '16@s',
    },
    buttonLabel: {
        fontWeight: '500',
        fontSize: '14@ms',
        color: CommonColors.mainText,
    },
    button: {
        width: '100%',
        height: '48@s',
        backgroundColor: CommonColors.activeTintColor,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: scale(30),
        ...ShadowStyle,
        borderRadius: '3@s',
    },
    reveal: {
        position: 'absolute', bottom: scale(8), right: 10,
    },
});
