import React, {Component} from 'react';
import {
    View,
    TouchableOpacity,
    ImageBackground,
    TouchableWithoutFeedback,
    Keyboard,
    Platform,
    Modal, Image,
    ScrollView,
} from 'react-native';
import ScaledSheet from '../../libs/reactSizeMatter/ScaledSheet';
import Header from '../../component/Header';
import {CommonColors, CommonSize, CommonStyles, ShadowStyle} from '../../utils/CommonStyles';
import BackButton from '../../component/BackButton';
import Text from '../../component/Text';
import {moderateScale, scale} from '../../libs/reactSizeMatter/scalingUtils';
import ClearIcon from '../../../assets/svg/close.svg';
import {SvgXml} from 'react-native-svg';
import FloatingLabelInput from '../../component/FloatingLabelInput';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ImagePicker from 'react-native-image-picker';
import DialogUtil from '../../utils/DialogUtil';
import Permissions from 'react-native-permissions';
import {hideLoading, showLoading} from '../../actions';
import {connect} from 'react-redux';
import rf from '../../libs/RequestFactory';
import uuidv1 from 'uuid/v1';
import AppConfig from '../../utils/AppConfig';
import {getDate, getTimeDay, getTimeStamp} from '../../utils/Filters';
import * as actions from '../../actions';
import store from '../../store';
import I18n from '../../i18n/i18n';
import ImageCommon from '../../component/Image';

const AVATAR_DEFAULT = 'https://www.w3schools.com/howto/img_avatar.png';

class EditProfileScreen extends Component {

    constructor(props, context) {
        super(props, context);
        // const {avatar, email, phone, display_name, birthday, sex} = {};
        this.state = {
            display_name: '',
            email: '',
            phone: '',
            avatar: null,
            file_choose: false,
        };
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Image style={styles.headerImage}
                               resizeMode={'cover'}
                               source={require('../../../assets/images/header_user.png')}/>
                        <View style={styles.avatarView}>
                            <View style={{...ShadowStyle}}>
                                <ImageCommon
                                    resizeMode={'cover'}
                                    style={styles.avatar}
                                    uri={'https://tophinhnen.com/wp-content/uploads/2018/03/hinh-nen-anime-girl-dang-yeu-7.jpg'}/>
                            </View>
                        </View>
                    </View>
                        <KeyboardAwareScrollView
                            enableOnAndroid
                            enableAutoAutomaticScroll={Platform.OS === 'ios'}
                            bounces={true}
                            extraHeight={scale(250)}
                            scrollEnabled={true}
                            showsVerticalScrollIndicator={true}
                           >
                            <View style={{
                                flex:1
                            }}>
                                <View style={{paddingHorizontal: scale(16)}}>
                                    <FloatingLabelInput label={I18n.t('app.display_name')}
                                                        value={this.state.display_name}
                                                        onChangeText={(display_name) => this.setState({display_name})}/>
                                    <FloatingLabelInput label={I18n.t('app.full_name')}
                                                        value={this.state.email}
                                                        onChangeText={(email) => this.setState({email})}
                                    />
                                    <FloatingLabelInput label={I18n.t('app.job_position')}
                                                        value={this.state.phone}
                                                        onChangeText={(phone) => this.setState({phone})}
                                    />
                                    <FloatingLabelInput label={I18n.t('app.phone_number')}
                                                        value={this.state.phone}
                                                        onChangeText={(phone) => this.setState({phone})}
                                                        keyboardType={'number-pad'}/>
                                </View>
                                <TouchableOpacity
                                    activeOpacity={0.7}
                                    style={styles.doneButton}>
                                    <Text style={styles.buttonLabel}>Done</Text>
                                </TouchableOpacity>
                            </View>
                        </KeyboardAwareScrollView>
                    {this.renderHeader()}
                </View>
            </TouchableWithoutFeedback>
        );
    }

    showImagePickerOptions = () => {
        // this.setState({disabled: true});
        let options = {
            title: I18n.t('app.change_avatar'),
            cancelButtonTitle: I18n.t('app.cancel'),
            takePhotoButtonTitle: I18n.t('app.take_photo'),
            chooseFromLibraryButtonTitle: I18n.t('app.choose_from_library'),
            noData: true,
            mediaType: 'photo',
            quality: 1,
            maxWidth: 800,
            maxHeight: 800,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            // this.setState({disabled: false});
            if (response && response.uri) {
                this.setState({
                    file_choose: true,
                    avatar: response.uri,
                    fileName: 'file.jpg',
                    fileType: 'image/jpg',
                });
            } else if (response && response.error) {
                if (Platform.OS === 'ios') {
                    if (response.error === 'Camera not available on simulator') {
                        DialogUtil.showMessageDialog('Camera not available on simulator');
                    } else if (response.error === 'Camera permissions not granted') {
                        DialogUtil.showMessageDialog('Camera permissions not granted. Do you want to switch to settings page ?', () => {
                            this.switchesUserToSettings();
                        });
                    } else if (response.error === 'Photo library permissions not granted') {
                        DialogUtil.showMessageDialog('Photo library permissions not granted. Do you want to switch to settings page ?', () => {
                            this.switchesUserToSettings();
                        });
                    }
                }
            }
        });
    };

    switchesUserToSettings = () => {
        Permissions.openSettings().then(data => {
            // console.warn(data);
        }).catch(error => {
            DialogUtil.showMessageDialog(error);
        });
    };

    renderSeparator = () => <View style={{width: '100%', height: 1, backgroundColor: CommonColors.border}}/>;

    renderAvatarHeader = () => {
        const {avatar, file_choose} = this.state;
        const uri = file_choose ? (avatar || AVATAR_DEFAULT) : (avatar ? AppConfig.getUrlImage(avatar) : AVATAR_DEFAULT);
        return (
            <View style={styles.avatarHeader}>
                <TouchableOpacity onPress={() => this.showImagePickerOptions()}>
                    <ImageBackground style={styles.avatar}
                                     source={{uri}}
                    >
                        <View style={{
                            backgroundColor: 'rgba(0, 0, 0, 0.5)',
                            alignItems: 'center',
                            justifyContent: 'center',
                            paddingVertical: scale(7),
                        }}>
                            <Text style={{fontSize: moderateScale(12), color: '#fff'}}>{I18n.t('app.edit')}</Text>
                        </View>
                    </ImageBackground>
                    {
                        avatar && <TouchableOpacity
                            onPress={() => this.deleteImage()}
                            style={styles.deleteImage}>
                            <View style={styles.deleteInnerImage}>
                                <SvgXml xml={ClearIcon}/>
                            </View>
                        </TouchableOpacity>
                    }
                </TouchableOpacity>
            </View>
        );
    };

    deleteImage = () => {
        DialogUtil.showDialog(I18n.t('app.remove_avatar'), () => {
            this.setState({avatar: null});
        });
    };

    renderLeftHeader = () => <BackButton/>;

    renderHeader = () => {
        return (
            <Header
                headerStyle={{position: 'absolute', width: '100%', backgroundColor: 'transparent'}}
                left={this.renderLeftHeader()}
                center={this.renderCenterHeader()}
                // right={this.renderRightHeader()}
            />
        );
    };

    renderCenterHeader = () => (
        <Text style={CommonStyles.headerTitle}>{I18n.t('app.edit_profile')}</Text>
    );

    renderRightHeader = () => {
        return (
            <TouchableOpacity onPress={() => this.saveChangeProfile()}>
                <Text style={styles.rightTitle}>{I18n.t('app.save')}</Text>
            </TouchableOpacity>
        );
    };

    saveChangeProfile = async () => {
        this.props.showLoading();
        let body = new FormData();
        body.append('file_name', uuidv1());
        body.append('file', {
            uri: this.state.avatar,
            name: 'file2.jpg',
            type: 'image/jpg',
        });
        const {file_choose, avatar, display_name, email, sex, date} = this.state;
        let responseUploadImage = null;
        try {
            if (file_choose) {
                responseUploadImage = await rf.getRequest('UserRequest').uploadFile(body);
            }
            const params = {
                address: 'address',
                address2: 'address2',
                avatar: responseUploadImage ? responseUploadImage.url : avatar,
                birthday: date ? getTimeStamp(date) : null,
                display_name: display_name,
                email: email,
                sex: sex.value,
            };
            const response = await rf.getRequest('UserRequest').updateProfile(params);
            store.dispatch(actions.updateUserInformation());
            this.props.hideLoading();
        } catch (e) {
            this.props.hideLoading();
            console.warn('error', e);
        }
    };
}

const styles = ScaledSheet.create({
    container: {
        flex: 1,
    },
    avatarHeader: {
        width: '100%',
        height: scale(160),
        backgroundColor: '#FFD5D5',
        alignItems: 'center',
        justifyContent: 'center',
    },
    rightTitle: {
        color: CommonColors.mainText,
    },
    deleteImage: {
        width: '32@s',
        height: '32@s',
        borderRadius: '16@s',
        backgroundColor: '#FFD5D5',
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        right: 0,
    },
    deleteInnerImage: {
        width: '26@s',
        height: '26@s',
        borderRadius: '13@s',
        backgroundColor: CommonColors.headerBarBgColor,
        alignItems: 'center',
        justifyContent: 'center',
    },
    setting: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: scale(16),
        marginBottom: scale(12),
    },
    catalogLabel: {
        fontSize: '16@ms',
        fontWeight: '500',
        marginLeft: '10@s',
    },
    header: {
        height: '200@s',
    },
    headerImage: {
        width: '100%',
        height: '140@s',
    },
    avatar: {
        width: '100@s',
        height: '100@s',
        borderRadius: '50@s',
        borderWidth: '5@s',
        borderColor: '#FFCC00',
    },
    avatarView: {
        flexDirection: 'row',
        position: 'absolute',
        alignSelf: 'center',
        bottom: '20@s',
    },
    doneButton: {
        width: '335@s',
        height: '40@s',
        backgroundColor: '#FF9500',
        borderRadius: '20@s',
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: '20@s',
        marginBottom: '40@s',
        marginTop: '30@s'
    },
    buttonLabel: {
        fontSize: '15@ms',
        color: '#ffffff',
        fontWeight: '600',
    },
});

const mapDispatchToProps = dispatch => ({
    showLoading: () => {
        dispatch(showLoading());
    },
    hideLoading: () => {
        dispatch(hideLoading());
    },
});

const mapStateToProps = state => ({
    user: state.user,
});


export default connect(mapStateToProps, mapDispatchToProps)(EditProfileScreen);
