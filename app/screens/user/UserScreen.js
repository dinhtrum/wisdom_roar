import React, {Component} from 'react';
import {View, Image, TouchableWithoutFeedback, TouchableOpacity, StatusBar, Alert} from 'react-native';
import ScaledSheet from '../../libs/reactSizeMatter/ScaledSheet';
import Text from '../../component/Text';
import ImageCommon from '../../component/Image';
import {ShadowStyle} from '../../utils/CommonStyles';
import NextIcon from '../../../assets/svg/next_right.svg';
import HistoryIcon from '../../../assets/svg/app/history.svg';
import EditProfileIcon from '../../../assets/svg/app/edit_profile.svg';
import ChangePasswordIcon from '../../../assets/svg/app/change_password.svg';
import SettingsIcon from '../../../assets/svg/app/settings.svg';
import LogoutIcon from '../../../assets/svg/app/logout.svg';
import {SvgXml} from 'react-native-svg';
import Navigator from '../../utils/Navigator'
import AppPreferences from '../../utils/AppPreferences';
import {connect} from 'react-redux';
import {hideLoading, logOut, showLoading} from '../../actions';
import _ from 'lodash';
import I18n from '../../i18n/i18n'

class UserScreen extends Component {

    render() {
        const {display} = this.props;
        const profile = _.get(this.props.user, 'profile', {});
        const display_name = _.get(profile, 'display_name', '');
        return (
            <View style={[styles.container,display ? {} : {display: 'none'}, {zIndex: -1}]}>
                {
                    display && <StatusBar
                        backgroundColor="transparent"
                        translucent
                        barStyle="light-content"
                    />
                }
                <View style={styles.header}>
                    <Image style={styles.headerImage}
                           resizeMode={'cover'}
                           source={require('../../../assets/images/header_user.png')}/>
                    <View style={styles.avatarView}>
                        <View style={{...ShadowStyle}}>
                            <ImageCommon
                                resizeMode={'cover'}
                                style={styles.avatar}
                                uri={'https://tophinhnen.com/wp-content/uploads/2018/03/hinh-nen-anime-girl-dang-yeu-7.jpg'}/>
                        </View>

                        <View style={styles.userName}>
                            <Text style={styles.name}>{display_name}</Text>
                            <Text>20 - 04- 1995</Text>
                        </View>
                    </View>
                </View>
                <TouchableOpacity>
                    <View style={styles.rowView}>
                        <SvgXml xml={HistoryIcon}/>
                        <Text style={styles.title}>{I18n.t('app.history')}</Text>
                        <SvgXml xml={NextIcon}/>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>Navigator.navigate('EditProfileScreen')}>
                    <View style={styles.rowView}>
                        <SvgXml xml={EditProfileIcon}/>
                        <Text style={styles.title}>{I18n.t('app.edit_profile')}</Text>
                        <SvgXml xml={NextIcon}/>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>Navigator.navigate('ChangePasswordScreen')}>
                    <View style={styles.rowView}>
                        <SvgXml xml={ChangePasswordIcon}/>
                        <Text style={styles.title}>{I18n.t('app.change_password')}</Text>
                        <SvgXml xml={NextIcon}/>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity>
                    <View style={styles.rowView}>
                        <SvgXml xml={SettingsIcon}/>
                        <Text style={styles.title}>{I18n.t('app.settings')}</Text>
                        <SvgXml xml={NextIcon}/>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this._logout()}>
                    <View style={styles.rowView}>
                        <SvgXml xml={LogoutIcon}/>
                        <Text style={styles.title}>{I18n.t('app.logout')}</Text>
                        <SvgXml xml={NextIcon}/>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }

    _logout = ()=>{
        Alert.alert(
            I18n.t('app.Alert'),
            I18n.t('app.confirm_logout'),
            [
                {
                    text: 'Cancel',
                    onPress: () => {},
                    style: 'cancel',
                },
                {text: 'OK', onPress: () => this._handleLogout()},
            ],
            {cancelable: false},
        );
    }

    _handleLogout = ()=>{
        this.props.showLoading();
        setTimeout(() => {
            this.props.hideLoading();
            AppPreferences.removeAccessToken();
            AppPreferences.removeAccountLogin();
          //  this.props.logOut();
            Navigator.navigate('LoginScreen')
        }, 1000);
    }
}

const styles = ScaledSheet.create({
    container: {
        flex: 1,
    },
    header: {
        height: '200@s',
        marginBottom: '30@s',
    },
    headerImage: {
        width: '100%',
        height: '140@s',
    },
    avatar: {
        width: '100@s',
        height: '100@s',
        borderRadius: '50@s',
        borderWidth: '5@s',
        borderColor: '#FFCC00',
    },
    avatarView: {
        flexDirection: 'row',
        position: 'absolute',
        left: '20@s',
        bottom: '20@s',
    },
    userName: {
        height: '100@s',
        flex: 1,
        justifyContent: 'center',
        paddingVertical: '20@s',
        marginLeft: '10@s',
    },
    name: {
        color: '#fff',
        fontSize: '17@ms',
        fontWeight: '600',
        marginBottom: '20@s',
    },
    rowView: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: '20@s',
        marginBottom: '30@s',
    },
    title: {
        fontSize: '15@ms',
        fontWeight: '500',
        color: '#000000',
        flex: 1,
        marginHorizontal: '16@s',
    },
});

const mapDispatchToProps = dispatch => ({
    logOut: (params) => {
        dispatch(logOut(params));
    },
    showLoading: () => {
        dispatch(showLoading());
    },
    hideLoading: () => {
        dispatch(hideLoading());
    },
});

const mapStateToProps = state => ({
    user: state.user,
});


export default connect(mapStateToProps, mapDispatchToProps)(UserScreen);

