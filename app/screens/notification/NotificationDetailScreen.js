import React, {Component} from 'react';
import {View, Text, ScrollView} from 'react-native';
import ScaledSheet from '../../libs/reactSizeMatter/ScaledSheet';
import Header from '../../component/Header';
import {CommonStyles} from '../../utils/CommonStyles';
import BackButton from '../../component/BackButton';
import HTML from 'react-native-render-html';

const EXAMPLE = `<div class=\\"meta-single\\">T&ecirc;n ch&iacute;nh thức:&nbsp;<strong>C&ocirc;ng ty Cổ phần Toyota Thăng Long</strong></div><div class=\\"meta-single\\">Địa chỉ: Số 316, đường Cầu Giấy, phường Dịch Vọng, quận Cầu Giấy, Tp. H&agrave; Nội</div><div class=\\"meta-single\\">Điện thoại: 024.3833.888 &ndash; Fax: 024.3833.1111</div><div class=\\"meta-single\\">Email: <a href=\\"mailto:hotro@toyotathanglong.com.vn\\">hotro@toyotathanglong.com.vn</a></div><div class=\\"meta-single\\">Website: <a href=\\"http://www.toyotathanglong.com.vn\\">www.toyotathanglong.com.vn</a></div><div class=\\"meta-single\\">Tổng Gi&aacute;m đốc: B&ugrave;i Việt Dũng</div><div class=\\"meta-single\\">Giấy chứng nhận ĐKKD số: 0101434765 do Sở KH &amp; ĐT H&agrave; Nội cấp lần đầu ng&agrave;y 17/01/2003</div><div class=\\"news-single\\"><div class=\\"the-content\\"><p>C&ocirc;ng ty Cổ Phần Toyota Thăng Long l&agrave; đại diện ch&iacute;nh thức của C&ocirc;ng ty &ocirc;t&ocirc; Toyota Việt nam, hoạt động kết hợp 3 chức năng:&nbsp;<strong>B&aacute;n h&agrave;ng, bảo dưỡng, sửa chữa v&agrave; Cung cấp phụ t&ugrave;ng ch&iacute;nh h&atilde;ng</strong>&nbsp;th&agrave;nh một hệ thống thống nhất. Mục đ&iacute;ch kinh doanh của C&ocirc;ng ty ch&uacute;ng t&ocirc;i l&agrave;&nbsp;<strong>&ldquo;Ph&aacute;t triển mang định hướng kh&aacute;ch h&agrave;ng&rdquo;</strong>&nbsp;được gắn liền với Phương ch&acirc;m h&agrave;nh động&nbsp;<strong>&ldquo;V&igrave; lợi &iacute;ch l&acirc;u d&agrave;i của kh&aacute;ch h&agrave;ng&rdquo;.</strong></p><p><img class=\\"wp-image-3292 alignright\\" src=\\"https://toyotathanglong.com.vn/wp-content/uploads/2017/08/20180309_094429.jpg\\" alt=\\"\\" width=\\"359\\" height=\\"263\\" /></p><p>Với ph&ograve;ng trưng b&agrave;y rộng 500m2 với 2 mặt tiền tr&ecirc;n một trong những con đường đ&ocirc;ng đ&uacute;c nhất H&agrave; nội, ch&uacute;ng t&ocirc;i đ&atilde; sử dụng thiết kế hiện đại nhất theo ti&ecirc;u chuẩn Toyota to&agrave;n cầu. Qu&iacute; kh&aacute;ch sẽ được trực tiếp tham khảo tất cả c&aacute;c chủng loại xe Toyota đang được trưng b&agrave;y ở đ&acirc;y v&agrave; đặc biệt qu&iacute; kh&aacute;ch sẽ được tiếp đ&oacute;n bởi c&aacute;c nh&acirc;n vi&ecirc;n b&aacute;n h&agrave;ng nhiệt t&igrave;nh, chuy&ecirc;n nghiệp v&agrave; tận tụy theo đ&uacute;ng phong c&aacute;ch Toyota .<br />Với kinh nghiệm 15 năm kinh doanh &ocirc;t&ocirc; v&agrave;&nbsp;<a title=\\"dịch vụ sửa chữa\\" href=\\"https://toyotathanglong.com.vn/dich-vu-sua-chua/\\">dịch vụ sửa chữa</a>&nbsp;xe &ocirc;t&ocirc; c&aacute;c loại v&agrave; từ khi c&ocirc;ng ty ch&uacute;ng t&ocirc;i được Toyota Việt nam chấp thuận cho l&agrave;m đại l&yacute; ủy quyền, ch&uacute;ng t&ocirc;i đ&atilde; định hướng v&agrave; x&acirc;y dựng c&ocirc;ng ty theo ti&ecirc;u chuẩn của Toyota to&agrave;n cầu cả về trang thiết bị v&agrave; con người. To&agrave;n bộ kỹ thuật vi&ecirc;n của c&ocirc;ng ty đ&atilde; được c&aacute;c chuy&ecirc;n gia của Toyota Việt nam đ&agrave;o tạo chuy&ecirc;n ng&agrave;nh về sủa chữa, bảo h&agrave;nh, bảo dưỡng xe &ocirc; t&ocirc; du lịch. Sau thời gian đ&agrave;o tạo, c&aacute;c kỹ thuật vi&ecirc;n của c&ocirc;ng ty đ&atilde; được cấp chứng chỉ.</p><p><img class=\\"wp-image-3293 alignleft\\" src=\\"https://toyotathanglong.com.vn/wp-content/uploads/2017/08/20180309_095239.jpg\\" alt=\\"\\" width=\\"400\\" height=\\"289\\" />&nbsp;</p><p>Trạm dịch vụ của Toyota Thăng Long bắt đầu đi v&agrave;o hoạt động từ năm 2004. Với đội ngũ Cố vấn dịch vụ v&agrave; kỹ thuật vi&ecirc;n chuy&ecirc;n nghiệp c&oacute; nhiều kinh nghiệm, phục vụ nhiệt t&igrave;nh chu đ&aacute;o, Toyota Thăng Long l&agrave; trạm dịch vụ c&oacute; quy m&ocirc; lớn nhất của Toyota tại Việt nam, được &aacute;p dụng những ti&ecirc;u chuẩn kỹ thuật v&agrave; m&aacute;y m&oacute;c hiện đại tr&ecirc;n to&agrave;n cầu. Trạm dịch vụ hai tầng với diện t&iacute;ch mặt bằng 5.500m2 &ndash; xưởng được thiết kế đ&aacute;p ứng c&ocirc;ng suất 1.800xe/ th&aacute;ng gồm 10 cầu n&acirc;ng, 32 khoang sửa chữa, 02 buồng sơn hấp hiện đại.</p><p>Kho phụ t&ugrave;ng với diện t&iacute;ch 250m2 chứa một lượng lớn phụ t&ugrave;ng mới v&agrave; ch&iacute;nh hiệu đ&aacute;p ứng mọi nhu cầu của qu&yacute; kh&aacute;ch h&agrave;ng về c&aacute;c chủng loại xe hiện c&oacute; tại Việt Nam. Kho phụ t&ugrave;ng được sắp xếp một c&aacute;ch khoa học v&agrave; được quản l&yacute; theo m&atilde; số tr&ecirc;n hệ thống m&aacute;y t&iacute;nh hiện đại</p><p><img class=\\"wp-image-3294 alignright\\" src=\\"https://toyotathanglong.com.vn/wp-content/uploads/2017/08/20180309_092842.jpg\\" alt=\\"\\" width=\\"406\\" height=\\"388\\" /><br />Trung t&acirc;m quan hệ kh&aacute;ch h&agrave;ng l&agrave; nơi tiếp nhận to&agrave;n bộ th&ocirc;ng tin v&agrave; &yacute; kiến đ&oacute;ng g&oacute;p của kh&aacute;ch h&agrave;ng về mua xe v&agrave; dịch vụ, nhằm đảm bảo mỗi kh&aacute;ch h&agrave;ng đều nhận được sự chăm s&oacute;c tốt nhất khi đến với ch&uacute;ng t&ocirc;i.<br />Với nền tảng vững chắc trong lĩnh vực kinh doanh v&agrave; dịch vụ sửa chữa &ocirc;t&ocirc;, c&ocirc;ng ty ch&uacute;ng t&ocirc;i đ&atilde; thiết lập được mối quan hệ rộng r&atilde;i với c&aacute;c đối t&aacute;c kinh doanh &ocirc;t&ocirc; v&agrave; nhiều kh&aacute;ch h&agrave;ng lớn tr&ecirc;n to&agrave;n quốc. Từ khi th&agrave;nh lập cho tới nay, c&ocirc;ng ty ch&uacute;ng t&ocirc;i đ&atilde; đạt được doanh thu b&aacute;n h&agrave;ng tr&ecirc;n 10.000 xe Toyota v&agrave; hơn 100.000 kh&aacute;ch h&agrave;ng dịch vụ.<br />Với th&agrave;nh t&iacute;ch phục vụ kh&aacute;ch h&agrave;ng xuất sắc, C&ocirc;ng ty CP Toyota Thăng long đ&atilde; được Nh&agrave; sản xuất &ndash; C&ocirc;ng ty &ocirc; t&ocirc; Toyota Việt Nam ghi nhận v&agrave; trao tặng danh hiệu&nbsp;<strong>&ldquo;Đại l&yacute; ho&agrave;n th&agrave;nh xuất sắc c&aacute;c chỉ ti&ecirc;u kinh doanh &ldquo;trong nhiều năm li&ecirc;n tiếp đến nay</strong>. C&ocirc;ng ty CP Toyota Thăng long hiện đang l&agrave; một trong nh&oacute;m&nbsp;<strong>Đại l&yacute; xuất sắc nhất của C&ocirc;ng ty &ocirc; t&ocirc; Toyota Việt Nam.</strong></p><p>C&ocirc;ng ty Cổ phần Toyota Thăng Long k&iacute;nh ch&uacute;c Qu&yacute; kh&aacute;ch mạnh khỏe, hạnh ph&uacute;c.</p><p><em>H&acirc;n hạnh được phục vụ Qu&yacute; kh&aacute;ch!</em></p></div></div>`

export default class NotificationDetailScreen extends Component {

    render() {
        return (
            <View style={styles.container}>
                {this.renderHeader()}
                <ScrollView>
                    <HTML html={EXAMPLE || `<html/>`} />
                </ScrollView>
            </View>
        );
    }

    renderHeader = () => {
        return (
            <Header
                center={this.renderCenterHeader()}
                left={this.renderLeftHeader()}
            />
        );
    };

    renderCenterHeader = () => (
        <Text style={CommonStyles.headerTitle}>Notifications Detail</Text>
    );

    renderLeftHeader = () => {
        return <BackButton/>;
    };
}

const styles = ScaledSheet.create({
    container: {
        flex: 1,
    },
});
