import React, {Component} from 'react';
import {View, Text, FlatList, TouchableWithoutFeedback} from 'react-native';
import ScaledSheet from '../../libs/reactSizeMatter/ScaledSheet';
import Header from '../../component/Header';
import {CommonColors, CommonStyles} from '../../utils/CommonStyles';
import BackButton from '../../component/BackButton';
import I18n from '../../i18n/i18n';
import {scale} from '../../libs/reactSizeMatter/scalingUtils';
import _ from 'lodash';
import Navigator from '../../utils/Navigator'

export default class NotificationScreen extends Component {

    render() {
        return (
            <View style={[styles.container]}>
                {this.renderHeader()}
                <FlatList
                    data={[{isUnRead: false}, {isUnRead: false}, {isUnRead: true}, {isUnRead: false}]}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={this.renderItem}
                    ItemSeparatorComponent={this.renderSeparator}
                />
            </View>
        );
    }

    renderSeparator = () => <View
        style={{width: '100%', height: 1, marginLeft: scale(46), backgroundColor: CommonColors.border}}/>;

    renderItem = ({item, index}) => {
        const isUnRead = _.get(item, 'isUnRead', true);
        return (
            <TouchableWithoutFeedback onPress={()=>Navigator.navigate('NotificationDetailScreen')}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    {<View style={isUnRead ? styles.unReadNotification : styles.readNotification}/>}
                    <View style={{paddingVertical: scale(16)}}>
                        <Text style={styles.timeNotification}>2h ago</Text>
                        <Text style={isUnRead ? styles.titleUnread : styles.titleRead}>#BOOKLIBRARY</Text>
                        <Text numberOfLines={1} style={styles.content}>You've been 2 days delayed deadline ret elayed
                            deadline ret </Text>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        );
    };

    renderHeader = () => {
        return (
            <Header
                center={this.renderCenterHeader()}
                left={this.renderLeftHeader()}
            />
        );
    };

    renderCenterHeader = () => (
        <Text style={CommonStyles.headerTitle}>Notifications</Text>
    );

    renderLeftHeader = () => {
        return <BackButton/>;
    };
}

const styles = ScaledSheet.create({
    container: {
        flex: 1,
    },
    unReadNotification: {
        width: '10@s',
        height: '10@s',
        borderRadius: '5@s',
        backgroundColor: '#339DFF',
        marginLeft: '20@s',
        marginRight: '16@s',
    },
    readNotification: {
        width: '10@s',
        height: '10@s',
        borderRadius: '5@s',
        backgroundColor: '#FFF',
        marginLeft: '20@s',
        marginRight: '16@s',
    },
    timeNotification: {
        fontSize: '11@ms',
        color: '#000000',
    },
    titleUnread: {
        fontSize: '20@ms',
        color: '#000000',
        fontWeight: 'bold',
        marginVertical: '10@s',
    },
    titleRead: {
        fontSize: '20@ms',
        color: '#000000',
        marginVertical: '10@s',
    },
    content: {
        fontSize: '15@ms',
        color: '#000000',
        flex: 1,
    },
});
