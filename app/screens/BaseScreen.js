import React, { Component } from 'react';
import {
  View, BackHandler, Alert, Platform,
} from 'react-native';
import I18n from '../i18n/i18n';
import EventList from '../utils/EventList';

export default class BaseScreen extends Component {
  static navigationOptions = {
    headerLeft: <View />,
  };

  _didFocusSubscription;

  _willBlurSubscription;

  navigate(screen, params) {
    const { navigation } = this.props;
    const { navigate } = navigation;
    navigate(screen, params);
  }

  componentDidMount() {
    const { navigation } = this.props;

    const dataEventHandlers = this.getDataEventHandlers();
    for (const event in dataEventHandlers) {
      const handler = dataEventHandlers[event];
      // window.EventBus.bind(event, handler);
    }

    if (Platform.OS === 'android' && navigation) {
      this._didFocusSubscription = navigation.addListener('didFocus', () => BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid));
    }

    if (Platform.OS === 'android' && navigation) {
      this._willBlurSubscription = navigation.addListener('willBlur', () => BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid));
    }

  }

  componentWillUnmount() {
    const dataEventHandlers = this.getDataEventHandlers();
    for (const event in dataEventHandlers) {
      const handler = dataEventHandlers[event];
      // window.EventBus.unbind(event, handler);
    }

    if (Platform.OS === 'android') {
      if (this._didFocusSubscription) this._didFocusSubscription.remove();
      if (this._willBlurSubscription) this._willBlurSubscription.remove();
    }
  }

  getDataEventHandlers() {
    return {
      [EventList.RELOAD_SCREEN_DATA_LANGUAGE]: () => {
        this.forceUpdate();
      },
    };
  }

  notify(event, data) {
    // window.EventBus.notify(event, data);
  }

  onBackButtonPressAndroid = () => {
    const { navigation } = this.props;
    const mainScreens = ['HomeScreen', 'MarketsScreen', 'SalesPointScreen', 'TradingScreen', 'MarginScreen'];
    const index = mainScreens.indexOf(navigation.state.routeName);
    if (navigation.isFocused && navigation.isFocused() && index !== -1) {
      Alert.alert(
        I18n.t('exit.title'),
        I18n.t('exit.content'), [{
          text: I18n.t('exit.cancel'),
          onPress: () => { },
          style: 'cancel',
        }, {
          text: I18n.t('exit.ok'),
          onPress: () => BackHandler.exitApp(),
        }], {
          cancelable: false,
        },
      );
      return true;
    }
    return false;
  };
}
