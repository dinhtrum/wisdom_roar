import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Platform, ImageBackground} from 'react-native';
import {RNCamera} from 'react-native-camera';
import {moderateScale, scale, verticalScale} from '../libs/reactSizeMatter/scalingUtils';
import Navigator from '../utils/Navigator';
import rf from '../libs/RequestFactory';
import {SvgXml} from 'react-native-svg';
import Close from '../../assets/svg/clear_icon.svg';
import {hideLoading, showLoading} from '../actions';
import {connect} from 'react-redux';
import _ from 'lodash';
import {CommonColors, CommonSize, CommonStyles} from '../utils/CommonStyles';
import ScaledSheet from '../libs/reactSizeMatter/ScaledSheet';
import LottieView from 'lottie-react-native';

const flashModeOrder = {
    off: 'on',
    on: 'auto',
    auto: 'torch',
    torch: 'off',
};

const wbOrder = {
    auto: 'sunny',
    sunny: 'cloudy',
    cloudy: 'shadow',
    shadow: 'fluorescent',
    fluorescent: 'incandescent',
    incandescent: 'auto',
};

const data = {
    bounds: {
        size: {
            width: '135.1231231',
            height: '135.1231231',
        },
        origin: {
            x: 135.1234123,
            y: 245.12314214,
        },
    },
};

const landmarkSize = 2;

class BarCodeScreen extends React.Component {
    state = {
        flash: 'off',
        zoom: 0,
        autoFocus: 'on',
        depth: 0,
        type: 'back',
        whiteBalance: 'auto',
        ratio: '16:9',
        recordOptions: {
            mute: false,
            maxDuration: 5,
            quality: RNCamera.Constants.VideoQuality['288p'],
        },
        isRecording: false,
        canDetectFaces: false,
        canDetectText: false,
        canDetectBarcode: true,
        faces: [],
        textBlocks: [],
        barcodes: [],
        onlyOne: false,
        checkDisplay: false,
    };

    setFocusDepth(depth) {
        this.setState({
            depth,
        });
    }

    takePicture = async function () {
        if (this.camera) {
            const data = await this.camera.takePictureAsync();
            console.warn('takePicture ', data);
        }
    };

    takeVideo = async function () {
        if (this.camera) {
            try {
                const promise = this.camera.recordAsync(this.state.recordOptions);

                if (promise) {
                    this.setState({isRecording: true});
                    const data = await promise;
                    this.setState({isRecording: false});
                    console.warn('takeVideo', data);
                }
            } catch (e) {
                console.error(e);
            }
        }
    };

    toggle = value => () => this.setState(prevState => ({[value]: !prevState[value]}));

    facesDetected = ({faces}) => this.setState({faces});


    renderLandmarksOfFace(face) {
        const renderLandmark = position =>
            position && (
                <View
                    style={[
                        styles.landmark,
                        {
                            left: position.x - landmarkSize / 2,
                            top: position.y - landmarkSize / 2,
                        },
                    ]}
                />
            );
        return (
            <View key={`landmarks-${face.faceID}`}>
                {renderLandmark(face.leftEyePosition)}
                {renderLandmark(face.rightEyePosition)}
                {renderLandmark(face.leftEarPosition)}
                {renderLandmark(face.rightEarPosition)}
                {renderLandmark(face.leftCheekPosition)}
                {renderLandmark(face.rightCheekPosition)}
                {renderLandmark(face.leftMouthPosition)}
                {renderLandmark(face.mouthPosition)}
                {renderLandmark(face.rightMouthPosition)}
                {renderLandmark(face.noseBasePosition)}
                {renderLandmark(face.bottomMouthPosition)}
            </View>
        );
    }

    renderFaces = () => (
        <View style={styles.facesContainer} pointerEvents="none">
            {this.state.faces.map(this.renderFace)}
        </View>
    );

    renderLandmarks = () => (
        <View style={styles.facesContainer} pointerEvents="none">
            {this.state.faces.map(this.renderLandmarksOfFace)}
        </View>
    );

    renderTextBlocks = () => (
        <View style={styles.facesContainer} pointerEvents="none">
            {this.state.textBlocks.map(this.renderTextBlock)}
        </View>
    );


    textRecognized = object => {
        const {textBlocks} = object;
        this.setState({textBlocks});
    };

    barcodeRecognized = async ({barcodes}) => {
        // this.setState({
        //     onlyOne: true,
        // });
        if (!this.onlyOne) {
            this.onlyOne = true;
            const {data} = barcodes[0];
            this.setState({
                barcodes,
            });
            if (data) {
               this.handleScanQrCode(data)
            }
        }
    };

    handleScanQrCode = async (data)=>{
        const [book_id, item_id] = data.split('_');
        const username = _.get(this.props.user, 'username', '');
        try {
            const res = await rf.getRequest('HomeRequest').bookCheck({
                book_id,
                item_id,
                username,
            });
            this.setState({
                barcodes: [],
                checkDisplay: true,
            });
            this.animation.play();
            const {status, book} = res;
            setTimeout(() => {
                Navigator.navigate('BookDetailScreen', {book, status, book_id, item_id});
                this.setState({
                    checkDisplay: false
                })
            }, 1000);
        } catch (error) {
            console.warn(error);
        }
    }

    renderBarcodes = () => (
        <View style={styles.facesContainer} pointerEvents="none">
            {this.state.barcodes.map(this.renderBarcode)}
        </View>
    );

    renderBarcode = ({bounds, data, type}) => {
        return (
            <React.Fragment key={data + bounds.origin.x}>
                <ImageBackground
                    source={require('../../assets/images/scan_bar_code.png')}
                    resizeMode={'cover'}
                    style={[
                        styles.text,
                        {
                            width: parseFloat(bounds.size.width),
                            height: parseFloat(bounds.size.height),
                            left: parseFloat(bounds.origin.x),
                            top: parseFloat(bounds.origin.y),
                        },
                    ]}
                >
                    <Text style={[styles.textBlock]}>{`${data}`}</Text>
                </ImageBackground>
            </React.Fragment>
        );
    };

    renderCamera() {
        const {
            canDetectBarcode,
        } = this.state;
        return (
            <RNCamera
                ref={ref => {
                    this.camera = ref;
                }}
                style={{
                    flex: 1,
                }}
                type={this.state.type}
                flashMode={this.state.flash}
                autoFocus={this.state.autoFocus}
                zoom={this.state.zoom}
                whiteBalance={this.state.whiteBalance}
                ratio={this.state.ratio}
                focusDepth={this.state.depth}
                // trackingEnabled
                onBarCodeRead={this.barcodeReceived.bind(this)}
                androidCameraPermissionOptions={{
                    title: 'Permission to use camera',
                    message: 'We need your permission to use your camera',
                    buttonPositive: 'Ok',
                    buttonNegative: 'Cancel',
                }}
                onGoogleVisionBarcodesDetected={canDetectBarcode ? this.barcodeRecognized : null}
                googleVisionBarcodeType={RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeType.ALL}
            >
                {this.renderTitle()}
                {!!canDetectBarcode && this.renderBarcodes()}
            </RNCamera>
        );
    }

    barcodeReceived = async (response) => {
        if (Platform.OS === 'ios') {
            if (!this.onlyOne) {
                this.onlyOne = true;
                this.setState({
                    barcodes: [response],
                });
                if (response.data) {
                    this.handleScanQrCode(response.data)
                }
            }
        }
    };

    renderTitle = () => {
        return (
            <View
                style={{
                    flex: 0.5,
                }}
            >
                <Text style={{alignSelf: 'center', marginTop: scale(40), fontSize: moderateScale(22), color: 'white'}}>SCAN
                    QR BOOK</Text>
            </View>
        );
    };

    render() {
        return (
            <View style={styles.container}>
                {this.renderCamera()}
                {this.renderCheckSuccess()}
                <TouchableOpacity
                    onPress={() => Navigator.goBack()}
                    style={{
                        position: 'absolute',
                        alignSelf: 'center',
                        bottom: CommonSize.marginBottom,
                    }}>
                    <View style={styles.closeButton}>
                        <SvgXml xml={Close}/>
                    </View>
                    <Text style={styles.closeLabel}>Close</Text>
                </TouchableOpacity>
            </View>
        );
    }

    componentDidMount() {
        const {navigation} = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            this.onlyOne = false;
        });
    }

    renderCheckSuccess = () => {
        const {checkDisplay} = this.state;
        return (
            <View style={[{
                position: 'absolute',
                alignSelf: 'center',
                top: verticalScale(200),
                backgroundColor: 'transparent',
                width: scale(200),
                height: verticalScale(200),
            }, checkDisplay ? {} : {display: 'none'}]}>
                <LottieView
                    loop={false}
                    ref={animation => {
                        this.animation = animation;
                    }}
                    source={require('../../assets/checkmark-animation')}
                />
            </View>
        );
    };

    componentWillUnmount() {
        // Remove the event listener
        this.focusListener.remove();
    }
}

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
    },
    flipButton: {
        flex: 0.3,
        height: 40,
        marginHorizontal: 2,
        marginBottom: 10,
        marginTop: 10,
        borderRadius: 8,
        borderColor: 'white',
        borderWidth: 1,
        padding: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    flipText: {
        color: 'white',
        fontSize: 15,
    },
    zoomText: {
        position: 'absolute',
        bottom: 70,
        zIndex: 2,
        left: 2,
    },
    picButton: {
        backgroundColor: 'darkseagreen',
    },
    facesContainer: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        top: 0,
    },
    face: {
        padding: 10,
        borderWidth: 2,
        borderRadius: 2,
        position: 'absolute',
        borderColor: '#FFD700',
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    landmark: {
        width: landmarkSize,
        height: landmarkSize,
        position: 'absolute',
        backgroundColor: 'red',
    },
    faceText: {
        color: '#FFD700',
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 10,
        backgroundColor: 'transparent',
    },
    text: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textBlock: {
        color: '#FFF',
        position: 'absolute',
        textAlign: 'center',
        backgroundColor: 'transparent',
    },
    closeButton: {
        width: '48@s',
        height: '48@s',
        borderRadius: '24@s',
        backgroundColor: CommonColors.mainColor,
        alignItems: 'center',
        justifyContent: 'center',
    },
    closeLabel: {
        fontSize: '17@ms',
        color: '#FFFFFF',
        marginTop: '10@s',
    },
});

const mapStateToProps = state => ({
    user: state.user.profile,
});

const mapDispatchToProps = dispatch => ({
    showLoading: () => {
        dispatch(showLoading());
    },
    hideLoading: () => {
        dispatch(hideLoading());
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(BarCodeScreen);
