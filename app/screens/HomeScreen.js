import React, {Component} from 'react';
import {View, Image, Animated, TouchableWithoutFeedback, Dimensions, StatusBar} from 'react-native';
import ScaledSheet from '../libs/reactSizeMatter/ScaledSheet';
import {SvgXml} from 'react-native-svg';
import HomeIcon from '../../assets/svg/mainTabBar/home.svg';
import HomeActiveIcon from '../../assets/svg/mainTabBar/home_active.svg';
import MoreIcon from '../../assets/svg/mainTabBar/more.svg';
import MoreActiveIcon from '../../assets/svg/mainTabBar/more_active.svg';
import InnerMainIcon from '../../assets/svg/innerMainIcon.svg';
import Close from '../../assets/svg/clear_icon.svg';
import {scale, verticalScale} from '../libs/reactSizeMatter/scalingUtils';
import {CommonColors, ShadowStyle} from '../utils/CommonStyles';
import Navigator from '../utils/Navigator';
import UserScreen from './user/UserScreen';
import BookHomeScreen from './book/BookHomeScreen';

const {width, height} = Dimensions.get('window');

export default class HomeScreen extends Component {

    state = {
        mainIconAnim: new Animated.Value(0),
        fadeAnim: new Animated.Value(0),
        close: false,
        tabIndex: 0,
    };

    componentDidMount() {

    }

    onClick = () => {
       // this.mainTabAnimation(1);
        Navigator.navigate('BarCodeScreen', {
            isReturnBook: false,
        });

    };

    onClose = () => {
        //this.mainTabAnimation(0);
    };

    mainTabAnimation = (value) => {
        const {mainIconAnim, fadeAnim, close} = this.state;
        if (close) {
            setTimeout(() => {
                this.setState({
                    close: !close,
                });
            }, 300);
        } else {
            this.setState({
                close: !close,
            });
        }

        Animated.timing(
            mainIconAnim,
            {
                toValue: value,
                duration: 300,
            },
        ).start();
        Animated.timing(
            fadeAnim,
            {
                toValue: value,
                duration: 300,
            },
        ).start();

    };

    render() {
        const {close, tabIndex} = this.state;
        const borderRadius = this.state.mainIconAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [scale(5), scale(32)],
        });
        const opacity = this.state.fadeAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 0.5],
        });
        const display = close ? {} : {display: 'none'};
        return (
            <View style={styles.container}>
                <BookHomeScreen display={tabIndex === 0}/>
                <UserScreen display={tabIndex === 1}/>
                <View style={styles.bottomTabBar}>
                    <TouchableWithoutFeedback onPress={() => this.setState({tabIndex: 0})}>
                        <View style={styles.tabBarItem}>
                            {tabIndex !== 0 && <SvgXml xml={HomeIcon}/>}
                            {tabIndex === 0 &&
                            <View>
                                <SvgXml xml={HomeActiveIcon}/>
                                <View style={styles.indicatorTab}/>
                            </View>
                            }
                        </View>
                    </TouchableWithoutFeedback>

                    <View style={{flex: 1}}/>

                    <TouchableWithoutFeedback onPress={() => this.setState({tabIndex: 1})}>
                        <View style={styles.tabBarItem}>
                            {tabIndex !== 1 && <SvgXml xml={MoreIcon}/>}
                            {tabIndex === 1 &&
                            <View>
                                <SvgXml xml={MoreActiveIcon}/>
                                <View style={styles.indicatorTab}/>
                            </View>
                            }
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                {/*<Animated.View style={{*/}
                {/*    zIndex: 1,*/}
                {/*    backgroundColor: '#191919',*/}
                {/*    opacity: opacity,*/}
                {/*    height: close ? (height + scale(50)) : 0,*/}
                {/*    width: width,*/}
                {/*    alignItems: 'center',*/}
                {/*    justifyContent: 'center',*/}
                {/*    position: 'absolute',*/}
                {/*    ...display,*/}
                {/*}}/>*/}
                {/*<TouchableWithoutFeedback onPress={this._navigateReturnBookScreen}>*/}
                {/*    <Animated.Image style={[styles.returnBook, display, {*/}
                {/*        zIndex: 1,*/}
                {/*        opacity: this.state.fadeAnim, // Binds directly*/}
                {/*        transform: [{*/}
                {/*            translateY: this.state.fadeAnim.interpolate({*/}
                {/*                inputRange: [0, 1],*/}
                {/*                outputRange: [0, verticalScale(150)],  // 0 : 150, 0.5 : 75, 1 : 0*/}
                {/*            }),*/}
                {/*        }],*/}
                {/*    },*/}
                {/*    ]}*/}
                {/*                    resizeMode={'contain'}*/}
                {/*                    source={require('../../assets/images/returnBook.png')}/>*/}
                {/*</TouchableWithoutFeedback>*/}
                {/*<TouchableWithoutFeedback onPress={this._navigateBorrowScreen}>*/}
                {/*    <Animated.Image style={[styles.borrowBook, display,*/}
                {/*        !close ? {height: 0} : {height: scale(150)},*/}
                {/*        {*/}
                {/*            zIndex: 1,*/}
                {/*            opacity: this.state.fadeAnim, // Binds directly*/}
                {/*            transform: [{*/}
                {/*                translateY: this.state.fadeAnim.interpolate({*/}
                {/*                    inputRange: [0, 1],*/}
                {/*                    outputRange: [0, verticalScale(-180)],  // 0 : 150, 0.5 : 75, 1 : 0*/}
                {/*                }),*/}
                {/*            }],*/}
                {/*        }]}*/}
                {/*                    resizeMode={'contain'}*/}
                {/*                    source={require('../../assets/images/borrowBook.png')}/>*/}
                {/*</TouchableWithoutFeedback>*/}

                <TouchableWithoutFeedback onPress={() => !close ? this.onClick() : this.onClose()}>
                    <Animated.View
                        style={[styles.mainIcon, {borderRadius, zIndex: 2}]}>
                        {!close && <SvgXml style={{transform: [{rotate: '45deg'}]}} xml={InnerMainIcon}/>}
                        {close && <SvgXml style={{transform: [{rotate: '45deg'}]}} xml={Close}/>}
                    </Animated.View>
                </TouchableWithoutFeedback>
            </View>
        );
    }

    _navigateBorrowScreen = () => {
        Navigator.navigate('BarCodeScreen', {
            isReturnBook: false,
        });
        this.onClose()
    };

    _navigateReturnBookScreen = () => {
        Navigator.navigate('BarCodeScreen', {
            isReturnBook: true,
        });
       this.onClose()
    };
}

const styles = ScaledSheet.create({
    container: {
        flex: 1,
    },
    bottomTabBar: {
        flexDirection: 'row',
        height: '70@s',
        borderTopWidth: 0.5,
        borderColor: '#aaa',
        width: '100%',
        paddingTop: '10@s',
        backgroundColor: '#ffffff'
    },
    tabBarItem: {
        flex: 1,
        alignItems: 'center',
    },
    mainIcon: {
        backgroundColor: '#FF9500',
        width: '64@s',
        height: '64@s',
        position: 'absolute',
        transform: [{rotate: '45deg'}],
        borderRadius: '5@s',
        bottom: scale(38),
        alignSelf: 'center',
        shadowColor: 'rgba(255, 149, 0, 0.25)',
        shadowOpacity: 0.2,
        shadowOffset: {
            width: 10,
            height: 5,
        },
        elevation: 3,
        alignItems: 'center',
        justifyContent: 'center',
    },
    returnBook: {
        width: '335@s',
        height: '150@s',
        marginBottom: '50@s',
        position: 'absolute',
        alignSelf: 'center',
    },
    borrowBook: {
        width: '335@s',
        height: '150@s',
        position: 'absolute',
        alignSelf: 'center',
        bottom: 0,
    },
    indicatorTab: {
        width: '5@s',
        height: '5@s',
        borderRadius: '2.5@s',
        backgroundColor: CommonColors.mainColor,
        alignSelf: 'center',
        marginTop: '12@s',
    },
});
