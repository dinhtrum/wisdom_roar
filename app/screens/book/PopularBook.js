import React, {Component} from 'react';
import {View, TouchableOpacity, TouchableWithoutFeedback, ActivityIndicator, Platform} from 'react-native';
import ScaledSheet from '../../libs/reactSizeMatter/ScaledSheet';
import {CommonColors, Fonts, ShadowStyle} from '../../utils/CommonStyles';
import Image from '../../component/Image';
import {SvgXml} from 'react-native-svg';
import StarIcon from '../../../assets/svg/app/star.svg';
import {scale} from '../../libs/reactSizeMatter/scalingUtils';
import rf from '../../libs/RequestFactory';
import Text from '../../component/Text';
import Navigator from '../../utils/Navigator';
import RatingComponent from '../../component/RatingComponent';
import BookItem from './BookItem';
import I18n from '../../i18n/i18n'

export default class PopularBook extends Component {

    state = {
        bookList: [],
    };

    async componentDidMount() {
        try {
            const res = await rf.getRequest('HomeRequest').getBookList();
            this.setState({
                bookList: res,
            });
        } catch (e) {
            console.log('get book list error', e);
        }
    }

    render() {
        const {bookList} = this.state;
        const size = Platform.OS === 'ios' ? 'large' : scale(40);
        return (
            <View style={styles.container}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View style={styles.borderLeftPopular}/>
                    <Text style={styles.popularBookLabel}>{I18n.t('app.popular_book')}</Text>
                    <TouchableOpacity onPress={()=> {
                        Navigator.navigate('PopularBookListScreen')
                    }}>
                        <Text style={styles.showAll}>{I18n.t('app.show_all')}</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flex: 1}}>
                    {bookList.map((item, index) => {
                        return this.renderItem(item, index);
                    })}
                    {
                        bookList.length === 0 &&
                            <ActivityIndicator style={{ marginVertical: scale(40) }} animating size={size} color={CommonColors.mainColor} />
                    }
                </View>
            </View>
        );
    }

    renderItem = (item, index) => {
        return(
            <BookItem key={index} item={item}/>
        )
    };
}

const styles = ScaledSheet.create({
    container: {
        flex: 1,
    },
    borderLeftPopular: {
        width: '4@s',
        height: '28@s',
        backgroundColor: CommonColors.mainColor,
        borderTopRightRadius: '3@s',
        borderBottomRightRadius: '3@s',
    },
    popularBookLabel: {
        fontSize: '22@ms',
        fontWeight: 'bold',
        color: '#000000',
        marginHorizontal: '16@s',
        flex: 1,
    },
    showAll: {
        fontSize: '14@ms',
        fontWeight: '600',
        color: '#FF3B30',
        marginRight: '20@s',
    },

    bgBeHind: {
        height: '65@s',
        width: '335@s',
        backgroundColor: '#EFEFF4',
        borderTopLeftRadius: '5@s',
        borderTopRightRadius: '5@s',
        flexDirection: 'row',
    },
    bgBeHindDown: {
        height: '5@s',
        width: '335@s',
        backgroundColor: CommonColors.mainColor,
        borderBottomLeftRadius: '5@s',
        borderBottomRightRadius: '5@s',
        bottom: 0,
    },

});
