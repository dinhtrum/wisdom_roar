import React, {Component} from 'react';
import {View, Text, TouchableWithoutFeedback} from 'react-native';
import ScaledSheet from '../../libs/reactSizeMatter/ScaledSheet';
import Navigator from '../../utils/Navigator';
import Image from '../../component/Image';
import RatingComponent from '../../component/RatingComponent';
import {CommonColors, Fonts} from '../../utils/CommonStyles';
import {moderateScale, scale} from '../../libs/reactSizeMatter/scalingUtils';
import EyeIcon from '../../../assets/svg/app/eye.svg'
import {SvgXml} from 'react-native-svg';

export default class BookItem extends Component {

    render() {
        const {thumbnail, main_title, author, _id, introduction} = this.props.item;
        const image = thumbnail !== '-' ? thumbnail.split(',')[0] : null;
        return (
            <TouchableWithoutFeedback
                onPress={() => Navigator.navigate('BookDetailScreen', {id: _id})}>
                <View style={styles.popularItem}>
                    <View style={styles.imageShadow}>
                        <Image
                            uri={image}
                            resizeMode={'cover'}
                            style={styles.bookImage}/>
                        <View style={styles.numberBook}>
                            <Text style={{fontSize: moderateScale(11), color: '#FFCC00'}}><Text
                                style={{color: '#fff'}}>2</Text> / 5</Text>
                        </View>
                        <View style={styles.eyeBook}>
                            <SvgXml xml={EyeIcon}/>
                        </View>
                    </View>
                    <View style={{flex: 1}}>
                        <Text numberOfLines={2} style={styles.bookName}>{main_title}</Text>
                        <Text numberOfLines={1} style={styles.author}>{author}</Text>
                        <Text numberOfLines={3} style={styles.introduce}>{introduction}</Text>
                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: scale(10)}}>
                            <Text style={styles.bookName}><Text>Rating: </Text></Text>
                            <RatingComponent/>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = ScaledSheet.create({
    container: {
        flex: 1,
    },
    popularItem: {
        height: '120@s',
        flex: 1,
        marginHorizontal: '20@s',
        paddingTop: '5@s',
        marginTop: '30@s',
        flexDirection: 'row',
    },
    bookImage: {
        width: '95@s',
        height: '123@s',
        borderRadius: '3@s',
        marginRight: '16@s',
    },
    star: {
        flexDirection: 'row',
        marginTop: '25@s',
    },
    starNumber: {
        fontSize: '11@ms',
        color: '#FF3B30',
        fontWeight: '600',
        marginLeft: '7@s',
    },
    nameBook: {
        fontSize: '11@ms',
        fontWeight: '600',
        color: '#000000',
        ...Fonts.defaultBold,
    },
    author: {
        fontSize: '11@ms',
        color: '#8E8E93',
        marginVertical: '8@s',
    },
    imageShadow: {
        shadowColor: '#4b4b4b',
        shadowOpacity: 0.2,
        shadowOffset: {
            width: 5,
            height: 8,
        },
        elevation: 3,
    },
    bookName: {
        fontWeight: '600',
        fontSize: '11@ms',
        color: '#000',
        ...Fonts.defaultBold,
    },
    introduce: {
        fontSize: '11@ms',
        color: '#000',
    },
    numberBook: {
        width: '40@s',
        height: '16@s',
        borderTopLeftRadius: '8@s',
        borderBottomLeftRadius: '8@s',
        backgroundColor: CommonColors.mainColor,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        right: scale(16)
    },
    eyeBook: {
        width: '26@s',
        height: '16@s',
        borderTopRightRadius: '8@s',
        borderBottomRightRadius: '8@s',
        backgroundColor: '#E5E5EA',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        left: 0
    },
});
