import React, {Component} from 'react';
import {View, Text} from 'react-native';
import ScaledSheet from '../../libs/reactSizeMatter/ScaledSheet';
import FetchableList from '../../component/FetchableList';
import BookItem from './BookItem';
import Header from '../../component/Header';
import {CommonStyles} from '../../utils/CommonStyles';
import BackButton from '../../component/BackButton';
import I18n from '../../i18n/i18n'

export default class PopularBookListScreen extends Component {

    render() {
        return (
            <View style={styles.container}>
                {this.renderHeader()}
                <FetchableList endpoint={'/book/list'}
                               renderItem={this.renderItem}
                               keyExtractor={this.keyExtractor}
                />
            </View>
        );
    }

    renderHeader = () => {
        return (
            <Header
                center={this.renderCenterHeader()}
                left={this.renderLeftHeader()}
            />
        );
    };

    renderCenterHeader = () => (
        <Text style={CommonStyles.headerTitle}>{I18n.t('app.popular_book')}</Text>
    );

    renderLeftHeader = () => {
        return <BackButton/>;
    };

    keyExtractor = (item, index)=> index.toString();

    renderItem  = ({item})=>{
        return(
            <BookItem item={item}/>
        )
    }
}

const styles = ScaledSheet.create({
    container: {
        flex: 1,
    },
});
