import React, {Component} from 'react';
import {View, TouchableOpacity, StatusBar, ScrollView, Animated} from 'react-native';
import ScaledSheet from '../../libs/reactSizeMatter/ScaledSheet';
import {CommonColors, CommonSize, CommonStyles, ShadowStyle} from '../../utils/CommonStyles';
import Carousel from 'react-native-snap-carousel';
import SliderEntry from './SliderEntry';
import {itemWidth, sliderWidth} from './styles/SliderEntry';
import Text from '../../component/Text';
import {moderateScale, scale} from '../../libs/reactSizeMatter/scalingUtils';
import {SvgXml} from 'react-native-svg';
import NotificationIcon from '../../../assets/svg/app/NotificationIcon.svg';
import Navigator from '../../utils/Navigator';
import PopularBook from './PopularBook';
import rf from '../../libs/RequestFactory';
import {connect} from 'react-redux';
import _ from 'lodash';
import Consts from '../../utils/Consts';
import moment from 'moment'


const SLIDER_1_FIRST_ITEM = 1;
const scrollRangeForAnimation = 68;

class BookHomeScreen extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            scrollY: new Animated.Value(0),
        };
    }

    render() {
        const profile = _.get(this.props.user, 'profile', {});
        const display_name = _.get(profile, 'display_name', '');
        const arraySplit = display_name.split(' ');
        const name = arraySplit[arraySplit.length - 1];

        const {display} = this.props;
        const nameSize = this.state.scrollY.interpolate({
            inputRange: [0, 100],
            outputRange: [moderateScale(34), moderateScale(24)],
            extrapolate: 'clamp',
        });
        const margin = this.state.scrollY.interpolate({
            inputRange: [0, 100],
            outputRange: [scale(20), scale(10)],
            extrapolate: 'clamp',
        });
        return (
            <View
                style={[styles.container, display ? {} : {display: 'none'},
                    {paddingTop: CommonSize.paddingTopHeader}]}>
                <StatusBar
                    backgroundColor="transparent"
                    translucent
                    barStyle="dark-content"
                />
                <Animated.View style={[{
                    paddingHorizontal: scale(16),
                    marginTop: margin,
                    paddingBottom: margin,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    borderBottomWidth: 0.5,
                    borderColor: '#e2e2e2',
                }]}>
                    <View>
                        <Animated.Text style={styles.day}>{moment().format('LL')}</Animated.Text>
                        <Animated.Text style={[styles.helloName, {fontSize: nameSize}]}>Hi, {name}</Animated.Text>
                    </View>
                    <TouchableOpacity
                        onPress={() => Navigator.navigate('NotificationScreen')}
                        style={{alignSelf: 'flex-end', marginBottom: scale(10)}}>
                        <SvgXml xml={NotificationIcon}/>
                    </TouchableOpacity>
                </Animated.View>
                <Animated.ScrollView
                    bounces={false}
                    scrollEventThrottle={16}
                    onScroll={Animated.event(
                        [
                            {
                                nativeEvent: {contentOffset: {y: this.state.scrollY}},
                            },
                        ],
                    )}
                >
                    <View style={{height: scale(180), justifyContent: 'center', marginVertical: scale(30)}}>
                        <Carousel
                            ref={c => this._slider1Ref = c}
                            data={Consts.CATEGORY}
                            renderItem={this._renderItemWithParallax}
                            sliderWidth={sliderWidth}
                            itemWidth={itemWidth}
                            hasParallaxImages={true}
                            firstItem={SLIDER_1_FIRST_ITEM}
                            inactiveSlideScale={0.94}
                            inactiveSlideOpacity={0.7}
                            // inactiveSlideShift={20}
                            containerCustomStyle={styles.slider}
                            contentContainerCustomStyle={styles.sliderContentContainer}
                            loop={true}
                            loopClonesPerSide={2}
                            autoplay={false}
                            autoplayDelay={500}
                            autoplayInterval={3000}
                            onSnapToItem={(index) => this.setState({slider1ActiveSlide: index})}
                        />
                    </View>
                    <PopularBook/>
                    <View style={{height: scale(100)}}/>
                </Animated.ScrollView>
            </View>
        );
    }

    _renderItemWithParallax({item, index}, parallaxProps) {
        return (
            <SliderEntry
                data={item}
                even={(index + 1) % 2 === 0}
                parallax={true}
                parallaxProps={parallaxProps}
            />
        );
    }
}

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        paddingTop: CommonSize.paddingTopHeader,
    },
    sliderContentContainer: {
        paddingVertical: 10,
    },
    helloName: {
        fontSize: '34@ms',
        color: '#000000',
        fontWeight: 'bold',
    },
    day: {
        color: 'rgba(0, 0, 0, 0.394871)',
        fontSize: '13@ms',
        textTransform: 'uppercase',
        fontWeight: '600',
        marginBottom: '3@s',
    },
});

const mapStateToProps = state => ({
    user: state.user,
});

export default connect(mapStateToProps, null)(BookHomeScreen);
