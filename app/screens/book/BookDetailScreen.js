import React, {Component} from 'react';
import {
    View,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Image,
    ScrollView,
    Animated,
    Alert,
    Platform, ActivityIndicator,
} from 'react-native';
import ScaledSheet from '../../libs/reactSizeMatter/ScaledSheet';
import {CommonColors, CommonSize, CommonStyles, Fonts, ShadowStyle} from '../../utils/CommonStyles';
import {scale, verticalScale} from '../../libs/reactSizeMatter/scalingUtils';
import ImageCommon from '../../component/Image';
import {hideLoading, showLoading} from '../../actions';
import Navigator from '../../utils/Navigator';
import DialogUtil from '../../utils/DialogUtil';
import {connect} from 'react-redux';
import {SvgXml} from 'react-native-svg';
import StarIcon from '../../../assets/svg/app/star.svg';
import UserIcon from '../../../assets/svg/app/User.svg';
import EditIcon from '../../../assets/svg/app/Edit.svg';
import Text from '../../component/Text';
import Header from '../../component/Header';
import BackButton from '../../component/BackButton';
import rf from '../../libs/RequestFactory';
import _ from 'lodash';
import {getTimeDay} from '../../utils/Filters';
import VccLoading from '../../component/VccLoading';
import Utils from '../../utils/Utils';
import I18n from '../../i18n/i18n'

class BookDetailScreen extends Component {

    state = {
        tab: 0,
        scrollY: new Animated.Value(0),
        book: null,
    };

    async componentDidMount() {
        const book = _.get(this.props.navigation.state.params, 'book', null);
        if (book) {
            this.setState({
                book,
            });
        } else {
            const id = _.get(this.props.navigation.state.params, 'id', null);
            try {
                const book = await rf.getRequest('HomeRequest').getBookDetail(id);
                this.setState({
                    book,
                });
            } catch (e) {
                console.log('get book detail error', e);
            }
        }
    }

    render() {
        const {book} = this.state;
        const thumbnail = _.get(book, 'thumbnail', '');
        const image = thumbnail.split(',')[0];
        const main_title = _.get(book, 'main_title', '');
        const author = _.get(book, 'author', '');
        const category = _.get(book, 'category', '');
        const introduction = _.get(book, 'introduction', '');
        const proposer_review = _.get(book, 'proposer_review', '');
        const proposer = _.get(book, 'proposer', '');
        const release_company = _.get(book, 'release_company', '');
        const published_year = _.get(book, 'published_year', '');
        const isbn_code = _.get(book, 'isbn_code', '');
        const number_of_pages = _.get(book, 'number_of_pages', '');
        const amount = _.get(book, 'amount', 0);
        const available_item_ids = _.get(book, 'available_item_ids', []);
        const {tab} = this.state;
        const opacity = this.state.scrollY.interpolate({
            inputRange: [0, 100],
            outputRange: [0, 1],
            extrapolate: 'clamp',
        });
        const size = Platform.OS === 'ios' ? 'large' : scale(40);
        return (
            <View style={styles.container}>
                <Image
                    resizeMode={'cover'}
                    style={styles.headerImageBook}
                    source={require('../../../assets/images/header_book.png')}/>

                {
                    <ScrollView
                    bounces={false}
                    scrollEventThrottle={16}
                    onScroll={Animated.event(
                        [
                            {
                                nativeEvent: {contentOffset: {y: this.state.scrollY}},
                            },
                        ],
                    )}
                >
                    <View style={[styles.content]}>
                        <View style={styles.bookAvatarView}>
                            <ImageCommon
                                resizeMode={'cover'}
                                uri={image}
                                style={styles.bookAvatar}/>
                        </View>
                        <View style={[book ? {} :{display: 'none'}]}>
                            <Text style={styles.category}>{category}</Text>
                            <Text numberOfLines={3} style={styles.bookName}>{main_title}</Text>
                            <View style={styles.authorView}>
                                <Text style={styles.authorName}>{author}</Text>
                                <Text style={styles.separatorColumn}>/</Text>
                                <SvgXml xml={StarIcon}/>
                                <Text style={styles.starNumber}>5.0</Text>
                            </View>
                            <View style={styles.separator}/>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <TouchableWithoutFeedback onPress={() => this.setState({tab: 0})}>
                                    <View style={{flex: 1, alignItems: 'center'}}>
                                        <Text style={tab === 0 ? styles.tabLabelActive : styles.tabLabelUnActive}>{I18n.t('app.about_book')}</Text>
                                        {tab === 0 && <View style={styles.tabSeparator}/>}
                                    </View>
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={() => this.setState({tab: 1})}>
                                    <View style={{flex: 1, alignItems: 'center'}}>
                                        <Text
                                            style={tab === 1 ? styles.tabLabelActive : styles.tabLabelUnActive}>{I18n.t('app.reviews')}</Text>
                                        {tab === 1 && <View style={styles.tabSeparator}/>}
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>

                            <View>
                                <View>
                                    {this.renderHeaderTitle(I18n.t('app.introduction'))}
                                    <Text style={styles.introduce}>
                                        {introduction}
                                    </Text>
                                </View>
                                <View>
                                    {this.renderHeaderTitle(I18n.t('app.proposer_review'))}
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <SvgXml xml={UserIcon}/>
                                        <Text style={styles.proposer}>{proposer}</Text>
                                    </View>
                                    <View style={{flexDirection: 'row', marginTop: scale(20)}}>
                                        <SvgXml xml={EditIcon}/>
                                        <Text style={styles.proposer}>
                                            {proposer_review}
                                        </Text>
                                    </View>
                                </View>
                                <View>
                                    {this.renderHeaderTitle(I18n.t('app.book_information'))}
                                    {this.renderTableRow(true, I18n.t('app.issuing_company'),'', ()=>
                                        <Text style={styles.company}>{release_company.trim() || ''}</Text>
                                    )}
                                    {this.renderTableRow(false, I18n.t('app.author'), author.trim())}
                                    {this.renderTableRow(false, I18n.t('app.publication_date'), getTimeDay(published_year, 'MM-YYYY'))}
                                    {this.renderTableRow(false, 'ISBN-13', isbn_code.trim())}
                                    {/*{this.renderTableRow(false, 'Size', 'Penguin Random House')}*/}
                                    {/*{this.renderTableRow(false, 'Cover type', 'Penguin Random House')}*/}
                                    {this.renderTableRow(false, I18n.t('app.pages'), number_of_pages.trim())}
                                </View>
                            </View>
                        </View>
                        {
                            !book &&
                                <View style={{height: verticalScale(500), width: '100%', justifyContent: 'center', alignItems:'center'}}>
                                    <ActivityIndicator style={{ marginVertical: 4 }} animating size={size} color={CommonColors.mainColor} />
                                </View>
                        }
                    </View>

                </ScrollView>

                }
                <Animated.View style={[CommonStyles.header, {opacity: opacity, position: 'absolute', width: '100%'}]}>
                    <View style={[styles.center]}>
                        <Text numberOfLines={1} style={styles.centerTitle}>{main_title}</Text>
                    </View>
                </Animated.View>
                <View style={[CommonStyles.header, {
                    backgroundColor: 'transparent',
                    position: 'absolute',
                    width: '100%',
                }]}>
                    <View style={[styles.center]}>

                    </View>
                    <View style={[styles.left]}>
                        <BackButton/>
                    </View>
                    <View style={[styles.right]}>

                    </View>
                </View>
                <View style={styles.footer}>
                    <View style={{flex: 1, alignItems: 'center'}}>
                        <Text>On stock</Text>
                        <Text style={styles.stockLabel}>
                            <Text style={{color: CommonColors.decreased}}>{available_item_ids.length}</Text> / {amount}
                        </Text>
                    </View>
                    {this.renderButton()}
                </View>
            </View>
        );
    }

    renderButton = () => {
        const status = _.get(this.props.navigation.state.params, 'status', 2);
        if (status === 0) {
            return (
                <View style={styles.borrowButton}>
                    <Text style={styles.buttonLabel}>Đã hết sách</Text>
                </View>
            );
        } else if (status === 1) {
            return (
                <TouchableOpacity
                    onPress={() => this.confirmBorrowAction(I18n.t('app.you_want_borrow_this_book'),
                        () => this.handleBorrowBook())}
                    style={styles.borrowButton}>
                    <Text style={styles.buttonLabel}>{I18n.t('app.borrow_book')}</Text>
                </TouchableOpacity>
            );
        } else if (status === -1) {
            return (
                <TouchableOpacity
                    onPress={() => this.confirmBorrowAction(I18n.t('app.you_want_return_this_book'),
                        () => this.handleReturnBook())}
                    style={styles.returnButton}>
                    <Text style={styles.buttonLabel}>{I18n.t('app.return_book')}</Text>
                </TouchableOpacity>
            );
        } else if (status === 2) {
            return (
                <TouchableOpacity
                    onPress={() => Navigator.navigate('BarCodeScreen')}
                    style={styles.borrowButton}>
                    <Text style={styles.buttonLabel}>{I18n.t('app.borrow_book')}</Text>
                </TouchableOpacity>
            );
        }
    };

    confirmBorrowAction = (title, action) => {
        Alert.alert(
            'Confirm',
            title,
            [
                {
                    text: 'Cancel',
                    onPress: () => {
                    },
                    style: 'cancel',
                },
                {text: 'OK', onPress: () => action()},
            ],
            {cancelable: false},
        );
    };

    handleBorrowBook = async () => {
        const book_id = _.get(this.props.navigation.state.params, 'book_id', 1);
        const item_id = _.get(this.props.navigation.state.params, 'item_id', 1);
        const book = _.get(this.props.navigation.state.params, 'book', {});
        const username = _.get(this.props.user, 'username', null);
        this.props.showLoading();
        try {
            const response = await rf.getRequest('HomeRequest').bookCreate({
                book_id,
                item_id,
                username,
            });
            this.props.hideLoading();
            Navigator.resetScreen('BorrowBookScreen', {data: response, book});
        } catch (e) {
            this.props.hideLoading();
            const message = _.get(e, 'message', null);
            Utils.showErrorToast({message: message || I18n.t('app.borrow_book_failed')});
            console.log('get book detail error', e);
        }
    };

    handleReturnBook = async () => {
        const book_id = _.get(this.props.navigation.state.params, 'book_id', 1);
        const item_id = _.get(this.props.navigation.state.params, 'item_id', 1);
        const book = _.get(this.props.navigation.state.params, 'book', {});
        const username = _.get(this.props.user, 'username', null);
        this.props.showLoading();
        try {
            const response = await rf.getRequest('HomeRequest').bookReturn({
                book_id,
                item_id,
                username,
            });
            this.props.hideLoading();
            Navigator.resetScreen('ReturnBookScreen', {data: response, book});
        } catch (e) {
            this.props.hideLoading();
            Utils.showErrorToast(I18n.t('app.return_book_failed'));
        }
    };

    renderTableRow = (isDrawBorderTopWidth, tdLabel, trLabel, view) => {
        return (
            <View style={{flexDirection: 'row'}}>
                <View style={[styles.tdRow, {borderTopWidth: isDrawBorderTopWidth ? scale(1) : 0}]}>
                    <Text style={styles.tdLabel}>{tdLabel}</Text>
                </View>
                <View style={[styles.trRow, {borderTopWidth: isDrawBorderTopWidth ? scale(1) : 0}]}>
                    {
                        view ? view() : <Text style={styles.trLabel}>{trLabel || ''}</Text>
                    }
                </View>
            </View>
        );
    };

    renderHeaderTitle = (title) => {
        return (
            <View style={styles.headerTitle}>
                <View style={styles.separatorColumnHeader}/>
                <Text style={styles.headerTitleLabel}>{title}</Text>
            </View>
        );
    };

}

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        backgroundColor: CommonColors.screenBgColor,
    },
    footer: {
        height: '63@vs',
        width: '100%',
        shadowColor: '#999999',
        shadowOpacity: 0.2,
        shadowOffset: {
            width: 0,
            height: -5,
        },
        elevation: 3,
        backgroundColor: '#fff',
        flexDirection: 'row',
        paddingTop: '5@s',
    },
    content: {
        width: '359@s',
        marginHorizontal: '8@s',
        backgroundColor: '#fff',
        flex: 1,
        ...ShadowStyle,
        borderTopLeftRadius: '20@s',
        borderTopRightRadius: '20@s',
        marginTop: '140@vs',
        padding: '20@s',
    },
    bookAvatar: {
        width: '100@vs',
        height: '125@vs',
        backgroundColor: '#AAD6FF',
        borderRadius: '3@s',
    },
    bookName: {
        fontSize: '18@ms',
        color: '#000',
        alignSelf: 'center',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    authorName: {
        fontSize: '13@ms',
        color: '#8E8E93',
        fontWeight: '600',
        ...Fonts.defaultBold,
    },
    separatorColumn: {
        fontSize: '13@ms',
        color: '#000',
        marginHorizontal: '14@s',
    },
    separator: {
        backgroundColor: '#D1D1D6',
        width: '319@s',
        height: '1@s',
        alignSelf: 'center',
        marginTop: '20@s',
        marginBottom: '20@s',
    },
    buttonLabel: {
        fontSize: '15@ms',
        color: '#fff',
        fontWeight: '600',
        ...Fonts.defaultBold,
    },
    stockLabel: {
        fontSize: '15@ms',
        color: '#000000',
        fontWeight: '600',
        ...Fonts.defaultBold,
    },
    returnButton: {
        flex: 1,
        height: '40@s',
        backgroundColor: '#5AC8FA',
        alignItems: 'center',
        justifyContent: 'center',
        borderTopLeftRadius: '20@s',
        borderBottomLeftRadius: '20@s',
    },
    borrowButton: {
        flex: 1,
        height: '40@s',
        backgroundColor: CommonColors.mainColor,
        alignItems: 'center',
        justifyContent: 'center',
        borderTopLeftRadius: '20@s',
        borderBottomLeftRadius: '20@s',
    },
    headerImageBook: {
        width: '100%',
        height: '375@s',
        position: 'absolute',
    },
    category: {
        paddingHorizontal: '10@s',
        paddingVertical: '5@s',
        backgroundColor: 'rgba(255,149,0, 0.25)',
        color: '#FF9500',
        marginTop: '70@vs',
        borderRadius: '3@s',
        marginBottom: '17@vs',
        alignSelf: 'center',
        fontSize: '11@ms',
    },
    bookAvatarView: {
        position: 'absolute',
        top: -verticalScale(60.5),
        alignSelf: 'center',
        shadowColor: '#FF9500',
        shadowOpacity: 0.2,
        shadowOffset: {
            width: 2,
            height: 6,
        },
        elevation: 3,
    },
    starNumber: {
        fontSize: '13@ms',
        color: '#000',
        fontWeight: '600',
        marginLeft: '5@s',
    },
    authorView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: '17@vs',
        alignSelf: 'center',
    },
    tabLabelActive: {
        fontSize: '17@ms',
        fontWeight: '600',
        color: CommonColors.mainColor,
    },
    tabLabelUnActive: {
        fontSize: '17@ms',
        fontWeight: '600',
        color: '#D1D1D6',
    },
    tabSeparator: {
        width: '30@s',
        height: '3@s',
        backgroundColor: CommonColors.mainColor,
        borderRadius: '1.5@s',
        marginTop: '3@s',
    },
    separatorColumnHeader: {
        width: '3@s',
        height: '20@s',
        backgroundColor: '#FF9500',
        borderRadius: '1.5@s',
    },
    headerTitleLabel: {
        fontWeight: '600',
        fontSize: '16@ms',
        color: '#000',
        marginLeft: '10@s',
        ...Fonts.defaultBold,
    },
    introduce: {
        fontSize: '15@ms',
        color: '#000',
    },
    headerTitle: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: '30@s',
        marginBottom: '16@s',
    },
    proposer: {
        fontSize: '15@ms',
        color: '#000000',
        marginLeft: '15@ms',
        marginTop: '5@ms',
        flex: 1,
    },
    tdRow: {
        paddingVertical: '5@s',
        width: '130@s',
        backgroundColor: '#EFEFF4',
        justifyContent: 'center',
        paddingHorizontal: scale(10),
        borderWidth: scale(1),
        borderColor: CommonColors.border,
    },
    trRow: {
        paddingVertical: '5@s',
        flex: 1,
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        paddingHorizontal: scale(10),
        borderWidth: scale(1),
        borderLeftWidth: 0,
        borderColor: CommonColors.border,
    },
    tdLabel: {
        fontSize: '11@ms',
        fontWeight: '600',
        lineHeight: scale(21),
        ...Fonts.defaultBold,
    },
    trLabel: {
        fontSize: '11@ms',
        lineHeight: scale(21),
    },
    company: {
        fontSize: '11@ms',
        lineHeight: scale(21),
        color: '#007AFF'
    },
    left: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: scale(15),
    },
    right: {
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: scale(15),
    },
    center: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        top: CommonSize.paddingTopHeader,
        bottom: 0,
    },
    centerTitle: {
        color: '#fff',
        fontSize: '17@ms',
        fontWeight: '600',
        ...Fonts.defaultBold,
        width: '200@s',
        textAlign: 'center',
    },
});

const mapStateToProps = state => ({
    user: state.user.profile,
    loading: state.loading.loading,
});

const mapDispatchToProps = dispatch => ({
    showLoading: () => {
        dispatch(showLoading());
    },
    hideLoading: () => {
        dispatch(hideLoading());
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(BookDetailScreen);
