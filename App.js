import {
  createAppContainer,
  createStackNavigator,
  createSwitchNavigator,
} from 'react-navigation';
import {Provider} from 'react-redux';
import React from 'react';
import Navigator from './app/utils/Navigator';
import {Animated, Easing} from 'react-native';
import store from './app/store';
import AppPreferences from './app/utils/AppPreferences';
import I18n from './app/i18n/i18n';
import AppConfig from './app/utils/AppConfig';
import {StatusBar, Platform} from 'react-native';

import * as actions from './app/actions';
import {scale} from './app/libs/reactSizeMatter/scalingUtils';
import HomeScreen from './app/screens/HomeScreen';
import ReturnBookScreen from './app/screens/ReturnBookScreen';
import BorrowBookScreen from './app/screens/BorrowBookScreen';
import IntroduceScreen from './app/screens/IntroduceScreen';
import BarCodeScreen from './app/screens/BarCodeScreen';
import NotificationScreen from './app/screens/notification/NotificationScreen';
import NotificationDetailScreen from './app/screens/notification/NotificationDetailScreen';
import EditProfileScreen from './app/screens/user/EditProfileScreen';
import ChangePasswordScreen from './app/screens/user/ChangePasswordScreen';
import BookDetailScreen from './app/screens/book/BookDetailScreen';
import LoginScreen from './app/screens/login/LoginScreen';
import Splash from './app/screens/SplashScreen';
import SplashScreen from 'react-native-splash-screen';
import PopularBookListScreen from './app/screens/book/PopularBookListScreen';

async function initApp() {
  // window.EventBus = new EventBus();
  if (Platform.OS === 'android') {
    StatusBar.setTranslucent(true);
  }
  AppPreferences.getLocale().then(locale => {
    if (!locale) {
      locale = 'vn';
    }
    I18n.locale = locale;
    store.dispatch(actions.changeLanguage(locale));
  });

  return await initMasterData();
}

async function initMasterData() {
  const credentials = await AppPreferences.getAccessToken();
  AppConfig.ACCESS_TOKEN = credentials;
  if (AppConfig.isLogin()) {
    store.dispatch(actions.fetchUserInformation());
  }
}

const transitionConfig = () => ({
  transitionSpec: {
    duration: 300,
    easing: Easing.out(Easing.poly(4)),
    timing: Animated.timing,
  },
  screenInterpolator: sceneProps => {
    const {layout, position, scene} = sceneProps;
    const {index} = scene;

    const height = layout.initHeight;
    const translateY = position.interpolate({
      inputRange: [index - 1, index, index + 1],
      outputRange: [height, 0, 0],
    });

    const opacity = position.interpolate({
      inputRange: [index - 1, index - 0.99, index],
      outputRange: [0, 1, 1],
    });

    return {opacity, transform: [{translateY}]};
  },
});

const MAIN_TAB_BAR_HEIGHT = scale(50);
export {MAIN_TAB_BAR_HEIGHT};

const HomeTab = createStackNavigator(
  {
    Home: {screen: HomeScreen},
    ReturnBookScreen: {screen: ReturnBookScreen},
    BorrowBookScreen: {screen: BorrowBookScreen},
    IntroduceScreen: {screen: IntroduceScreen},
    BarCodeScreen: {screen: BarCodeScreen},
    NotificationScreen: {screen: NotificationScreen},
    NotificationDetailScreen: {screen: NotificationDetailScreen},
    EditProfileScreen: {screen: EditProfileScreen},
    ChangePasswordScreen: {screen: ChangePasswordScreen},
    BookDetailScreen: {screen: BookDetailScreen},
    PopularBookListScreen: {screen: PopularBookListScreen},
  },
  {
    headerMode: 'none',
    initialRouteName: 'Home',
    cardStyle: {
      opacity: 1,
    },
    transitionConfig: Platform.OS === 'ios' ? null : transitionConfig,
  },
);

const MainScreenNavigator = createSwitchNavigator(
  {
    HomeTab: HomeTab,
    LoginScreen: LoginScreen,
    Splash: Splash,
  },
  {
    headerMode: 'none',
    cardStyle: {
      opacity: 1,
    },
    initialRouteName: 'Splash',
    transitionConfig: Platform.OS === 'ios' ? null : transitionConfig,
  },
);

const Navigation = createAppContainer(MainScreenNavigator);

export default class App extends React.Component {
  componentDidMount() {
    SplashScreen.hide();
  }

  render() {
    return (
      <Provider store={store}>
        <Navigation
          ref={navigatorRef => {
            Navigator.setTopLevelNavigator(navigatorRef);
          }}
        />
      </Provider>
    );
  }
}

export {initApp};
